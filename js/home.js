$('document').ready( function(){

	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	if(isiPad && Math.abs(window.orientation) != 90){
		window.location = 'http://www.aqfl.com.br/mobile/home';
	}
	
	$(window).bind( 'orientationchange', function(e){
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		if(isiPad && Math.abs(window.orientation) != 90){
			window.location = 'http://www.aqfl.com.br/mobile/home';
		}
	});

	if(window.innerHeight < 700 && $.browser.msie){
		$('html body #main #menu-container nav ul').css('margin-top', '30%');
	}

	/* Animação dos marcadores dos itens do menu */
	$('nav li a').hover( function(){

		largura_menu = parseInt($('#menu-container').css('width')) / 2;
		largura_link = parseInt($(this).css('width')) / 2;
		largura = largura_menu - largura_link - 9;
		
		$($(this).parent().find('.marcador-left, .marcador-right')).stop().animate({
			width   : largura,
			opacity : 1
		}, 300);
		
	}, function(){

		if(!$(this).hasClass('ativo')){
			$($(this).parent().find('.marcador-left, .marcador-right')).stop().animate({
				width   : 0,
				opacity : 0
			}, 300);
		}

	});

	$('#loading_depoimento .windows8').css('top', (window.innerHeight / 2) -20);

	$('#info-contato').css('top', .7 * window.innerHeight);

	/* Iniciar depoimentos */
	$('#galeria-depoimentos').live('click', function(e){
		e.preventDefault();
		proximoDepoimento();
	});

	/* Navegação dos depoimentos */
	$('#dep-nav a, #dep-start').live('click', function(e){
		e.preventDefault();
		proximoDepoimento($(this));
	});

	/* Botão de voltar das galeria de imagem full screen */
	$('#link-logo a').live('click', function(e){
		e.preventDefault();
		fechaGaleria($(this));
	});

	/* Botão para Iniciar o vídeo de Affair */
	$('#start-affair').live('click', function(e){
		e.preventDefault();
		mostraVideoAffair();
	});	

	/* Botão para fechar o vídeo Inicial */
	$('#logo_link_inicial a').live('click', function(e){
		e.preventDefault();
		fechaVideoInicio();
	});		

	/* Comportamento dos links da seção Inspirações */
	$('#tiles-inspiracoes a').live('click', function(e){
		e.preventDefault();
		if(!$('#main').hasClass('animando')){
			switch($(this).attr('id')){
				case 'celebracoes-link':
					abreGaleriaCelebracoes();
					break;
				case 'expressoes-link':
					abreGaleriaExpressoes();
					break;
				case 'affair-link':
					abreAffair();
					break;
			}
		}
	});

	/*
	 * Comportamento dos itens do menu 
	 * Ao clicar em um item, é necessário fechar o item atual e abrir a animação
	 * TODO: Executar a função para fechar somente para o item que estiver aberto
	  		 utilizando um marcador na div #main (podendo assim controlar os links do menu
	  		 	para não abrirem se já estiverem abertos DUH)
	*/
	$('.menu-link').live('click', function(e){
		e.preventDefault();
		
		if(!$(this).hasClass('ativo') || ($(this).hasClass('ativo') && $('#main').attr('data-secao') == 'affair')){

			if(!$('body').hasClass('animando')){
				
				switch($('#main').attr('data-secao')){
					case 'inicial':
						fechaInicial();
						break;
					case 'nos':
						fechaItemNos();
						break;
					case 'inspiracoes':
						fechaItemInspiracoes();
						break;
					case 'palavras':
						fechaItemPalavras();
						break;
					case 'contato':
						fechaItemContato();
						if(!$('#info-contato').hasClass('hid'))
							$('#info-contato').addClass('hid');
						break;
					case 'affair':
						fechaAffair();
						break;
				}
				
				var link = $(this);
				var id_link = $(this).attr('id');
				
				setTimeout( function(){

					if($('#main').attr('data-secao') != 'affair'){
						$('.ativo').removeClass('ativo').trigger('mouseout', function(){});
						link.addClass('ativo');
					}
					
					switch(id_link){
						case 'menu-inicio':
							$('#main').attr('data-secao', 'inicio');
							abreVideoInicio();
							break;
						case 'menu-nos':
							$('#main').attr('data-secao', 'nos');
							abreItemNos();
							break;
						case 'menu-inspiracoes':
							$('#main').attr('data-secao', 'inspiracoes');
							abreItemInspiracoes();
							break;
						case 'menu-palavras':
							$('#main').attr('data-secao', 'palavras');
							abreItemPalavras();
							break;
						case 'menu-contato':
							$('#main').attr('data-secao', 'contato');
							abreItemContato();
							break;
					}

				}, 100);
			}
		}
	});

	/* Centraliza verticalmente a mensagem de 'Carregando...' e faz o preload das imagens */
	$('#Vcenter').vAlign().fadeIn(1500, function(){

		imgList = [
			/* Imagens de Tile 20% da Home */
			BASE+'_imgs/layout/home1.jpg',
			BASE+'_imgs/layout/home2.jpg',
			BASE+'_imgs/layout/home3.jpg',
			BASE+'_imgs/layout/home4.jpg',
			/* Imagens de Poster dos Vídeos */
			BASE+'_imgs/inicial-poster.jpg',
			BASE+'_imgs/layout/play.png',
			/* Imagem de fundo de Nós */
			BASE+'_imgs/slides/slide1.jpg',
			/* Imagens de Tile de Inspirações*/
			BASE+'_imgs/layout/inspiracoes1.jpg',
			BASE+'_imgs/layout/inspiracoes2.jpg',
			BASE+'_imgs/layout/inspiracoes3.jpg',
			/* Imagem de fundo de Affair */
			BASE+'_imgs/layout/affair.jpg',
			/* Imagens de Tile de Contato */
			BASE+'_imgs/layout/contato1.jpg',
			BASE+'_imgs/layout/contato2.jpg',
			/* Imagens de navegação e logo */
			BASE+'_imgs/layout/marca.png',
			BASE+'_imgs/layout/next.png',
			BASE+'_imgs/layout/next-arrow.png',
			BASE+'_imgs/layout/plus-sign.png',
			BASE+'_imgs/layout/prev.png',
			BASE+'_imgs/layout/prev-arrow.png'
		];
		
		$.preLoadImages(
			imgList, function(){
				abreInicio();
		});
	});

});