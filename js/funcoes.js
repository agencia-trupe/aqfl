/* Definição do método .map() para browsers que não suportam  */
if(!Array.prototype.map){
	Array.prototype.map = function(callback, thisArg) {  
	  var T, A, k; 

	  if (this == null) {  
	    throw new TypeError(" this is null or not defined");  
	  }  

	  var O = Object(this);  

	  var len = O.length >>> 0;  

	  if ({}.toString.call(callback) != "[object Function]") {  
	    throw new TypeError(callback + " is not a function");  
	  }  

	  if (thisArg) {  
	    T = thisArg;  
	  }  

	  A = new Array(len);  
	  k = 0;  

	  while(k < len) {  

	    var kValue, mappedValue;  

	    if (k in O) {  

	      kValue = O[ k ];  

	      mappedValue = callback.call(T, kValue, k, O);  

	      A[ k ] = mappedValue;  
	    }  

	    k++;  
	  }

	  return A;  
	};
}

$.fn.vAlign = function() {
	return this.each(function(i){
		var ah = $(this).height();
		var ph = $(this).parent().height();
		var mh = Math.ceil((ph-ah) / 2);
		$(this).css('margin-top', mh);
	});
};

var isEventSupported = (function(){
  var TAGNAMES = {
    'select':'input','change':'input',
    'submit':'form','reset':'form',
    'error':'img','load':'img','abort':'img'
  }
  function isEventSupported(eventName) {
    var el = document.createElement(TAGNAMES[eventName] || 'div');
    eventName = 'on' + eventName;
    var isSupported = (eventName in el);
    if (!isSupported) {
      el.setAttribute(eventName, 'return;');
      isSupported = typeof el[eventName] == 'function';
    }
    el = null;
    return isSupported;
  }
  return isEventSupported;
})();

//===================================================//

function abreInicio(){
	if(!$('#menu-container').hasClass('hid'))
		$('#menu-container').addClass('hid');
	
	/* Bind ao término do vídeo para mostrar imagens */
	if($('#big-video-vid_html5_api').length && $('#big-video-vid_html5_api')[0].addEventListener){
		$('#big-video-vid_html5_api')[0].addEventListener('ended', fechaVideoInicio ,false);
	}else if($('#big-video-vid_flash_api').length && $('#big-video-vid_flash_api')[0].addEventListener){
		$('#big-video-vid_flash_api')[0].addEventListener('ended', fechaVideoInicio ,false);
	}

	$('#loading').delay(300).fadeOut(1800, 'easeOutExpo', function(){
		abreVideoInicio();
	});	
}

function abreVideoInicio(){
	$('#main').attr('data-secao', 'inicial');

	if(isiPad)
		$('#dog-play').fadeIn();
	
	if(!$('#big-video-vid').is(':visible'))
		$('#big-video-vid').fadeIn('normal');

	if(!$('#menu-container').hasClass('hid'))
		$('#menu-container').addClass('hid');

	if($('.tile:visible').length)
		$('.tile').addClass('hid');

	$('#logo_link_inicial').show('drop');
	BV.getPlayer().play();	
}

function fechaVideoInicio(){
	BV.getPlayer().pause();
	if(!isiPad)
		BV.getPlayer().currentTime(0);
	else
		$('#dog-play').fadeOut();
	$('#big-video-vid').fadeOut('normal');
	$('#logo_link_inicial').hide('drop');
	$('.ativo').removeClass('ativo');
	mostraMenu();		
}

function fechaInicial(){
	
	if(!$('#tiles-inicio .tile#imagem-noivos').hasClass('hid')){

		if(!$('body').hasClass('animando'))
			$('body').addClass('animando');

		if (typeof(fecha) != 'undefined')
        	clearTimeout(fecha);

		var fecha = setTimeout( function(){
			$('#tiles-inicio .tile#imagem-noivos').addClass('hid');
				if(!$('#tiles-inicio .tile#imagem-altar').hasClass('hid')){
					setTimeout( function(){
						$('#tiles-inicio .tile#imagem-altar').addClass('hid');
							if(!$('#tiles-inicio .tile#imagem-alianca').hasClass('hid')){
								setTimeout( function(){
									$('#tiles-inicio .tile#imagem-alianca').addClass('hid');
										if(!$('#tiles-inicio .tile#imagem-noiva').hasClass('hid')){
											$('#tiles-inicio .tile#imagem-noiva').addClass('hid');
											if(!$('#tiles-inicio').hasClass('hid')){
					 							setTimeout( function(){
													$('#tiles-inicio').addClass('hid');
													if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
														$("#tiles-inicio").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
															$('body').removeClass('animando');
														});
													}else{
														$('body').removeClass('animando');
													}
					 							}, 150);
					 						}
										}
								}, 150);
							}
					}, 150);
				}
		}, 150);
	}	
}

function mostraMenu(){

	if(!$('body').hasClass('animando'))
		$('body').addClass('animando');

	var abre = setTimeout( function(){
		$('#tiles-inicio').removeClass('hid');
		setTimeout( function(){
			$('#tiles-inicio .tile#imagem-noiva').removeClass('hid');
			setTimeout( function(){
				$('#tiles-inicio .tile#imagem-alianca').removeClass('hid');
			 	setTimeout( function(){
			 		$('#tiles-inicio .tile#imagem-altar').removeClass('hid');
			 		setTimeout( function(){
			 			$('#tiles-inicio .tile#imagem-noivos').removeClass('hid');
			 			$('#menu-container').removeClass('hid');
			 			if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
							$("#tiles-inicio .tile#imagem-noivos").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
								$('body').removeClass('animando');
							});
						}else{
							$('body').removeClass('animando');
						}						
			 		}, 150);
			 	}, 150);
			}, 150);
		}, 150);
	}, 150);
}

//===================================================//

function abreItemNos(){

	if(!$('body').hasClass('animando'))
		$('body').addClass('animando');

	if($('#texto-nos').css('height') == '0px'){
		var altura_texto = window.innerHeight - parseInt($('nav ul li a.ativo').parent().find('.marcador-right').offset().top) - 1;
		$('#texto-nos').css('height', altura_texto);
	}

	var abre = setTimeout( function(){
		$('#galeria-nos').removeClass('hid');
		setTimeout( function(){
			$('#texto-nos').removeClass('hid');
			if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
				$("#texto-nos").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
					$('body').removeClass('animando');
				});
			}else{
				$('body').removeClass('animando');
			}
		}, 150);
	}, 150);
}

function fechaItemNos(){

	if(!$('#texto-nos').hasClass('hid')){

		if(!$('body').hasClass('animando'))
			$('body').addClass('animando');

		if (typeof(fecha) != 'undefined')
        	clearTimeout(fecha);

		var fecha = setTimeout( function(){
			$('#texto-nos').addClass('hid');
				if(!$('#galeria-nos').hasClass('hid')){
					setTimeout( function(){
						$('#galeria-nos').addClass('hid')
						$('#texto-nos').css('height', 0);
						if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
							$("#galeria-nos").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
								$('body').removeClass('animando');
							});
						}else{
							$('body').removeClass('animando');
						}						
					}, 150);
				}
		}, 150);
	}
}

//===================================================//

function abreItemContato(){

	if(!$('body').hasClass('animando'))
		$('body').addClass('animando');

	var abre = setTimeout( function(){
		$('#tiles-contato').removeClass('hid');
		setTimeout( function(){
			$('#tiles-contato .tile#contato-esquerda').removeClass('hid');
			setTimeout( function(){
				$('#tiles-contato .tile#contato-direita').removeClass('hid');
			 	setTimeout( function(){
			 		$('#info-contato').removeClass('hid');
		 			if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
						$("#info-contato").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
							$('body').removeClass('animando');
						});
					}else{
						$('body').removeClass('animando');
					}						
			 	}, 150);
			}, 150);
		}, 150);
	}, 150);
}

function fechaItemContato(){

	if(!$('#tiles-contato').hasClass('hid')){

		if(!$('body').hasClass('animando'))
			$('body').addClass('animando');

		if (typeof(fecha) != 'undefined')
        	clearTimeout(fecha);

		var fecha = setTimeout( function(){
			$('#tiles-contato').addClass('hid');
				if(!$('#tiles-contato .tile#contato-direita').hasClass('hid')){
					setTimeout( function(){
						$('#tiles-contato .tile#contato-direita').addClass('hid');
							if(!$('#tiles-contato .tile#contato-esquerda').hasClass('hid')){
								setTimeout( function(){
									$('#tiles-contato .tile#contato-esquerda').addClass('hid');
										if(!$('#tiles-contato').hasClass('hid')){
				 							setTimeout( function(){
												$('#tiles-contato').addClass('hid');
												if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
													$("#tiles-contato").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
														$('body').removeClass('animando');
													});
												}else{
													$('body').removeClass('animando');
												}
				 							}, 150);
				 						}
								}, 150);
							}
					}, 150);
				}
		}, 150);
	}
}

//===================================================//

function abreItemInspiracoes(){

	if(!$('body').hasClass('animando'))
		$('body').addClass('animando');

	$('#tile-celebracoes a').css('width', 0.28 * window.innerWidth);

	var abre = setTimeout( function(){
		$('#tiles-inspiracoes').removeClass('hid');
		setTimeout( function(){
			$('#tiles-inspiracoes .tile#tile-celebracoes').removeClass('hid');
			setTimeout( function(){
				$('#tiles-inspiracoes .tile#tile-expressoes').removeClass('hid');
			 	setTimeout( function(){
			 		$('#tiles-inspiracoes .tile#tile-affair').removeClass('hid');
			 		if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
						$("#tiles-inspiracoes .tile#tile-affair").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
							$('body').removeClass('animando');
						});
					}else{
						$('body').removeClass('animando');
					}
			 	}, 150);
			}, 150);
		}, 150);
	}, 150);
}

function fechaItemInspiracoes(){
	if(!$('#tiles-inspiracoes .tile#tile-affair').hasClass('hid')){

		if(!$('body').hasClass('animando'))
			$('body').addClass('animando');

		if (typeof(fecha) != 'undefined')
        	clearTimeout(fecha);

		var fecha = setTimeout( function(){
			$('#tiles-inspiracoes .tile#tile-affair').addClass('hid');
				if(!$('#tiles-inspiracoes .tile#tile-expressoes').hasClass('hid')){
					setTimeout( function(){
						$('#tiles-inspiracoes .tile#tile-expressoes').addClass('hid');
							if(!$('#tiles-inspiracoes .tile#tile-celebracoes').hasClass('hid')){
								setTimeout( function(){
									$('#tiles-inspiracoes .tile#tile-celebracoes').addClass('hid');
										if(!$('#tiles-inspiracoes').hasClass('hid')){
				 							setTimeout( function(){
												$('#tiles-inspiracoes').addClass('hid');
												if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
													$("#tiles-inspiracoes").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
														$('body').removeClass('animando');
													});
												}else{
													$('body').removeClass('animando');
												}												
				 							}, 150);
				 						}
								}, 150);
							}
					}, 150);
				}
		}, 150);
	}
}

//===================================================//

function abreItemPalavras(){

	if(!$('body').hasClass('animando'))
		$('body').addClass('animando');

	if($('#texto-depoimentos').css('height') == '0px'){
		var altura_texto = window.innerHeight - parseInt($('nav ul li a.ativo').parent().find('.marcador-right').offset().top) - 1;
		$('#texto-depoimentos').css('height', altura_texto);
	}

	var abre = setTimeout( function(){
		$('#galeria-depoimentos').removeClass('hid');
		setTimeout( function(){
			$('#texto-depoimentos').removeClass('hid');
			if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
				$("#texto-depoimentos").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
					$('body').removeClass('animando');
				});
			}else{
				$('body').removeClass('animando');
			}					
		}, 150);
	}, 150);
}

function fechaItemPalavras(){

	if(!$('#texto-depoimentos').hasClass('hid')){

		if(!$('body').hasClass('animando'))
			$('body').addClass('animando');

		if (typeof(fecha) != 'undefined')
        	clearTimeout(fecha);

        proximoDepoimento();

		var fecha = setTimeout( function(){
			$('#texto-depoimentos').addClass('hid');
				if(!$('#galeria-depoimentos').hasClass('hid')){
					setTimeout( function(){
						$('#galeria-depoimentos').addClass('hid')
						$('#texto-depoimentos').css('height', 0);
						if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
							$("#galeria-depoimentos").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
								$('body').removeClass('animando');
							});
						}else{
							$('body').removeClass('animando');
						}
					}, 150);
				}
		}, 150);
	}
}

//===================================================//

var requestState = 0;

function proximoDepoimento(self){

	$('#main').attr('data-secao', 'palavras');

	try{
		id = self.attr('data-next');
	}catch(e){
		id = 'land';
	}

	if(requestState == 0){

		requestState = 1;

		$.post(BASE+'ajax/depoimentos', { id : id }, function(retorno){

			if(id == 'land'){
				retorno = {
					'arquivo' : 'capa.jpg',
					'casal' : 'Adoramos OUVIR HISTÓRIAS também!',
					'texto' : 'Aqui, abrimos espaço para que nossos tão queridos amigos e clientes contem um pouco das suas, e de como ao abrirem suas famílias e histórias para nós, nossos contos entraram onde sempre buscamos estar: no coração. Obrigado por tanto carinho.',
					'prev' : '0',
					'next' : '0'
				}
			}else{
				retorno = JSON.parse(retorno);
			}

			var img_path = '_imgs/depoimentos/'+retorno.arquivo;

			$('#loading_depoimento').fadeIn('slow');

			$.preLoadImages(
				img_path, function(){

					$('#loading_depoimento').fadeOut('slow');

					var slides = "<img src='"+img_path+"'>";

					$('#dep-start').fadeOut();

					$('#texto-depoimentos').find('.tit').fadeOut('normal', function(){
						$(this).html(retorno.casal);
					});

					$('#texto-depoimentos').find('.dep').fadeOut('normal', function(){
						$(this).html(retorno.texto);
					});

					$('#galeria-depoimentos').prepend(slides).find('img').waitForImages( function(){

						$('#galeria-depoimentos img:last').fadeOut(1000, function(){

							$('#galeria-depoimentos img').filter(':last').remove();

							$('#texto-depoimentos').find('.tit').fadeIn();
							$('#texto-depoimentos').find('.dep').fadeIn();

							if(id == 'land'){
								$('#dep-nav').fadeOut();
								$('#dep-start').fadeIn();
							}else{
								$('#dep-nav').fadeIn();
								$('#dep-start').fadeOut();
							}

							requestState = 0;

						});
					});

					$('#dep-nav').fadeIn('slow', function(){
						$('#dep-prev').attr('data-next', retorno.prev);
						$('#dep-next').attr('data-next', retorno.next);	
					});

			});
			
		});
	}
}

//===================================================//

function abreGaleriaCelebracoes(){

	$('#menu-container').addClass('hid');

	$.post(BASE+'ajax/abreGaleriaCelebracoes', function(retorno){
		
		fechaItemInspiracoes();

		var html;
		var altura_util   = window.innerHeight;
		var largura_util  = window.innerWidth;
		var ratio         = largura_util / altura_util;
		var contador = 0;
		var imgList = new Array();

		retorno = JSON.parse(retorno);

		html = "<div class='img-container'>";

		retorno.map( function(self){

			if(contador < 10)
				imgList.push('_imgs/galerias/celebracoes/'+self.arquivo);

			html += "<img src='_imgs/galerias/celebracoes/"+self.arquivo+"'>";

			contador++;
		});

		html += "</html>";

		$('#loading').fadeIn('slow');

		$.preLoadImages(
			imgList, function(){

			$('#loading').fadeOut('slow');
				
			if(isiPad){
				efeito = 'scrollHorz';
				timeout = 16000;
			}else{
				efeito = 'fade';
				timeout = 4000;
			}
			
			$('#galeria-celebracoes').html(html).removeClass('hid').find('.img-container').cycle({ 
				fx     : efeito,
				timeout: timeout,
				next   : $('#controle_slides .next'),
				prev   : $('#controle_slides .prev')
			});

			$('#bg_slides').on('swipeleft', function(){
				$('#bg_slides').cycle('next');
			});

			$('#bg_slides').on('swiperight', function(){
				$('#bg_slides').cycle('prev');
			});			

			$('#link-logo').removeClass('hid');
			$('#controle_slides').fadeIn('slow');

		});
		
	});
}

function abreGaleriaExpressoes(){

	$('#menu-container').addClass('hid');

	$.post(BASE+'ajax/abreGaleriaExpressoes', function(retorno){
		
		fechaItemInspiracoes();

		var html;
		var altura_util   = window.innerHeight;
		var largura_util  = window.innerWidth;
		var ratio         = largura_util / altura_util;
		var contador = 0;
		var imgList = new Array();

		retorno = JSON.parse(retorno);

		html = "<div class='img-container'>";

		retorno.map( function(self){

			if(contador < 10)
				imgList.push('_imgs/galerias/expressoes/'+self.arquivo);

			html += "<img src='_imgs/galerias/expressoes/"+self.arquivo+"'>";

			contador++;
		});

		html += "</html>";

		$('#loading').fadeIn('slow');

		$.preLoadImages(
			imgList, function(){

			$('#loading').fadeOut('slow');
		
			if(isiPad){
				efeito = 'scrollHorz';
				timeout = 16000;
			}else{
				efeito = 'fade';
				timeout = 4000;
			}

			$('#galeria-expressoes').html(html).removeClass('hid').find('.img-container').cycle({ 
				fx     : efeito,
				timeout: timeout,
				next   : $('#controle_slides .next'),
				prev   : $('#controle_slides .prev')
			});

			$('#bg_slides').on('swipeleft', function(){
				$('#bg_slides').cycle('next');
			});

			$('#bg_slides').on('swiperight', function(){
				$('#bg_slides').cycle('prev');
			});			

			$('#link-logo').removeClass('hid');
			$('#controle_slides').fadeIn('slow');

		});
	});	
}

//===================================================//

function abreAffair(){
	$('#main').attr('data-secao', 'affair');
	
	fechaItemInspiracoes();
	
	if(!$('body').hasClass('animando'))
		$('body').addClass('animando');

	if($('#texto-affair').css('height') == '0px'){
		var altura_texto = window.innerHeight - parseInt($('nav ul li a.ativo').parent().find('.marcador-right').offset().top) - 1;
		$('#texto-affair').css('height', altura_texto);
	}

	var abre = setTimeout( function(){
		$('#galeria-affair').removeClass('hid');
		setTimeout( function(){
			$('#texto-affair').removeClass('hid');
			if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
				$("#texto-affair").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
					$('body').removeClass('animando');
				});
			}else{
				$('body').removeClass('animando');
			}			
		}, 150);
	}, 150);	
}

function fechaAffair(){
	if(!$('#texto-affair').hasClass('hid')){

		if(!$('body').hasClass('animando'))
			$('body').addClass('animando');

		if (typeof(fecha) != 'undefined')
        	clearTimeout(fecha);

		var fecha = setTimeout( function(){
			$('#texto-affair').addClass('hid');
				if(!$('#galeria-affair').hasClass('hid')){
					setTimeout( function(){
						$('#galeria-affair').addClass('hid')
						$('#texto-affair').css('height', 0);
						if (isEventSupported('transitionend') || isEventSupported('webkitTransitionEnd') || isEventSupported('oTransitionEnd') || isEventSupported('MSTransitionEnd')) {
							$("#galeria-affair").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
								$('body').removeClass('animando');
							});
						}else{
							$('body').removeClass('animando');
						}								
					}, 150);
				}
		}, 150);
	}
}

//===================================================//

function mostraVideoAffair(){
	var altura_util   = window.innerHeight;
	var largura_util  = window.innerWidth;

	$('#video-affair').fadeIn('slow', function(){
		if(isiPad)
			var loading = $('<img id="playbtn" style="width:40px;" src="_imgs/layout/play.png">')
		else
			var loading = $("<div id='floatingCirclesG-Affair'><div class='f_circleG' id='frotateG_01'></div><div class='f_circleG' id='frotateG_02'></div><div class='f_circleG' id='frotateG_03'></div><div class='f_circleG' id='frotateG_04'></div><div class='f_circleG' id='frotateG_05'></div><div class='f_circleG' id='frotateG_06'></div><div class='f_circleG' id='frotateG_07'></div><div class='f_circleG' id='frotateG_08'></div></div>");

		var html = "<video id='affair' class='video-js vjs-default-skin' preload='auto' poster='' data-setup='{}' width='"+largura_util+"px' height='"+altura_util+"px'>";
		html += "<source src='http://player.vimeo.com/external/49263155.sd.mp4?s=19ad86880756a19620816170fa7795c6' type='video/mp4'>";
		html += "</video>";

		var ah = 60;
		var aw = 60;
		var ph = altura_util;
		var pw = largura_util;
		var mh = Math.ceil((ph-ah) / 2);
		
		var mw = Math.ceil((pw-aw) / 2);
		
		loading.css({
			top      : mh+'px',
			left     : mw+'px'
		});

		$(this).append(html).append(loading);
		$('#affair')[0].load();

		if(isiPad)
			control = true;
		else
			control = false;

		var myPlayer = _V_("affair");

		if(isiPad){
			$('#playbtn').bind('click', function(){
				loading.remove();
				myPlayer.play();
			});
		}else{
			loading.fadeOut(8000, function(){
				$(this).remove();
				myPlayer.play();
			});
		}

  		/* Botão para Fechar o vídeo de Affair */
		$('#video-affair a').live('click', function(e){

			e.preventDefault();
			myPlayer.pause();
			if(myPlayer.techName == "html5"){        
			    myPlayer.tag.src = "";                 
			    myPlayer.tech.removeTriggers();        
				myPlayer.load();                       
			}   
			myPlayer.tech.destroy();
			myPlayer.destroy();                      
			$(myPlayer.el).remove();
			$('#video-affair').fadeOut('slow', function(){
				delete($('#video-affair video')[0]);
				$('#floatingCirclesG-Affair').remove();
				if($('#video-affair video').length)
					$('#video-affair video').remove();
			});

		});
    	
	});
}

//===================================================//

function fechaGaleria(){
	$('#link-logo').addClass('hid');
	$('#menu-container').removeClass('hid');
	abreItemInspiracoes();
	$('.galeria:visible').addClass('hid');
	$('#controle_slides').fadeOut('slow');
	
	largura_menu = parseInt($('#menu-container').css('width')) / 2;
	largura_link = parseInt($(this).css('width')) / 2;
	largura = largura_menu - largura_link - 9;
	
	$($('#menu-inspiracoes').parent().find('.marcador-left, .marcador-right')).stop().animate({
		width   : largura,
		opacity : 1
	}, 300);
}
