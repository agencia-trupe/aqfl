/* Definição do método .map() para browsers que não suportam  */
if(!Array.prototype.map){
	Array.prototype.map = function(callback, thisArg) {  
	  var T, A, k; 

	  if (this == null) {  
	    throw new TypeError(" this is null or not defined");  
	  }  

	  var O = Object(this);  

	  var len = O.length >>> 0;  

	  if ({}.toString.call(callback) != "[object Function]") {  
	    throw new TypeError(callback + " is not a function");  
	  }  

	  if (thisArg) {  
	    T = thisArg;  
	  }  

	  A = new Array(len);  
	  k = 0;  

	  while(k < len) {  

	    var kValue, mappedValue;  

	    if (k in O) {  

	      kValue = O[ k ];  

	      mappedValue = callback.call(T, kValue, k, O);  

	      A[ k ] = mappedValue;  
	    }  

	    k++;  
	  }

	  return A;  
	};
}

/* Mini plugin - alinhamento vertical */
$.fn.vAlign = function() {
	return this.each(function(i){
		var ah = $(this).height();
		var ph = $(this).parent().height();
		var mh = Math.ceil((ph-ah) / 2);
		$(this).css('margin-top', mh);
	});
};

/* Imagens da animação da home */
var imgs_first_set = [
	BASE+'_imgs/layout/home2.jpg',
	BASE+'_imgs/layout/home1.jpg',
	BASE+'_imgs/layout/home3.jpg',
	BASE+'_imgs/layout/home4.jpg'
];
var imgs_second_set = [
	BASE+'_imgs/layout/home5.jpg',
	BASE+'_imgs/layout/home6.jpg',
	BASE+'_imgs/layout/home7.jpg',
	BASE+'_imgs/layout/home8.jpg'
];

/* Transição padrão para as animações */
var TRANSICAO = 'easeInOutQuart';

/* Animação para mostrar as imagens ao término do vídeo na home. */
function mostrarImagens(){
	$('#main').addClass('animando');

	var toShow = $('.tile').filter(':hidden');

	if(toShow.length){

		var atual = toShow.filter(':first');
		var margem = 0;

		var visible_width = 0;
		$('.tile:visible').each( function(){
			visible_width += parseInt($(this).css('width'));
		});
		var speed = 600;

		toShow.filter(':first')
			  .css({
			  	'margin-left' : window.innerWidth - visible_width,
			  	'opacity'	  : 0.2,
			  	'display'	  : 'block'
			  });

		toShow.filter(':first').find('img').css( { 'margin' : 0 , 'opacity' : 1 });

		toShow.filter(':first')
			  .animate({
			  	'margin-left' : 0,
			  	'opacity' 	  : 1
			  }, speed, TRANSICAO, function(){
					mostrarImagens();
			  });	
	

	}else{
		$('#main').removeClass('animando');
		$('.marcador-left, .marcador-right').css({ width:0, opacity:0 });
		$('#menu-container').fadeIn('slow');
	}
}

/* Animação para esconder as imagens ao término do vídeo na home. */
function escondeImagens(){
	$('#main').addClass('animando');
	var toHide = $('.tile').filter(':visible');
	if(toHide.length){

		var atual = toHide.filter(':last');
		var margem = parseInt(atual.find('img').css('width'));

		toHide.filter(':last').find('img').animate({ 'margin-left' : -1 * margem, 'opacity' : 0 }, 600, TRANSICAO, function(){
			$(this).parent('.tile').hide(0, function(){
				escondeImagens();
				if(!$('.tile').filter(':visible').length)
					$('#menu-container').delay(150).animate({ left : 0 }, 400);
			});
		});
	}else{
		$('#main').removeClass('animando');
	}
};

/* Ajusta o Tamanho do vídeo de acordo com a proporção de tela do navegador. */
function maximizaVideo(){
	var video        = $('#video video');
	var altura_util  = window.innerHeight;
	var largura_util = window.innerWidth;
	var ratio;

	ratio = largura_util / altura_util;
	if(ratio < 1.8)
		video.height = altura_util;
	else
		video.width = largura_util;
};

/* 
 Reposiciona a imagem mais larga após a animação do menu e
 Troca o set de imagens a serem mostradas ao término do vídeo na home.
*/
function trocaImagens(img_set){

	if($('.tile.canv.swap').length){
		$('#tile-first').before($('.tile.canv.swap'));
		$('.tile.canv.swap').removeClass('swap');		
	}

	i=0;
	$('.tile').each( function(){
		$(this).find('img').attr('src', img_set[i]);
		i++;
	});
}

/* 
   Define qual o set de imagens a ser mostrado usando como parametro uma classe
   no container do vídeo (first_set|second_set).
   Chama a função para fazer a troca e a função para começar a animação.
*/
function novasImagens(){
	var video = ($('#big-video-vid_html5_api').length) ? $('#big-video-vid_html5_api') : $('#big-video-vid_flash_api');
	var mostrar;

	if(video.hasClass('first_set')){
		video.removeClass('first_set').addClass('second_set');
		mostrar = imgs_second_set;
	}else{
		video.addClass('first_set').removeClass('second_set');
		mostrar = imgs_first_set;
	}
	trocaImagens(mostrar);
	mostrarImagens();
}

/*
	Inicia animação após o carregamento.
	Esconde a div de carregamento e esconde as imagens que já vem com display:block
*/
function iniciaAnimacaoHome(){
	//maximizaVideo();
	$('.tile-contato').fadeOut('slow');

	//$('.tile').fadeIn('slow', TRANSICAO);

	/* Bind ao término do vídeo para mostrar imagens */
	if($('#big-video-vid_html5_api').length && $('#big-video-vid_html5_api')[0].addEventListener){
		$('#big-video-vid_html5_api')[0].addEventListener('ended', fechaVideoInicio ,false);
	}else if($('#big-video-vid_flash_api').length && $('#big-video-vid_flash_api')[0].addEventListener){
		$('#big-video-vid_flash_api')[0].addEventListener('ended', fechaVideoInicio ,false);
	}

	$('#loading').delay(300).fadeOut(1800, 'easeOutExpo', function(){
		abreVideoInicio();
	});
}

function abreVideoInicio(){
	$('#main').attr('data-secao', 'inicial');

	if(isiPad)
		$('#dog-play').fadeIn();		
	
	if(!$('#big-video-vid').is(':visible'))
		$('#big-video-vid').fadeIn('normal');

	if($('#menu-container').is(':visible'))
		$('#menu-container').hide('drop');
	if($('.tile').is(':visible'))
		escondeImagens();

	$('#logo_link_inicial').show('drop');
	BV.getPlayer().play();

}

function fechaVideoInicio(){
	BV.getPlayer().pause();
	if(!isiPad)
		BV.getPlayer().currentTime(0);
	$('#big-video-vid').fadeOut('normal');
	$('#logo_link_inicial').hide('drop');
	$('.ativo').removeClass('ativo');
	mostrarImagens();	
}

/* Após acessar algum item do menu a Animação as imagens da home deve parar, mas o vídeo deve continuar em loop */
function paraAnimacaoHome(){
	$('.tile').fadeOut('slow', TRANSICAO);
	$('#main').removeClass('animando');
	if($('#big-video-vid_html5_api').length){
		if($('#big-video-vid_html5_api').length && $('#big-video-vid_html5_api')[0].removeEventListener){
			$('#big-video-vid_html5_api')[0].removeEventListener('ended', novasImagens ,false);
			$('#big-video-vid').remove();
			if(typeof BV != 'undefined')
				delete BV;
		}
	}else if($('#big-video-vid_flash_api').length){
		if($('#big-video-vid_flash_api').length && $('#big-video-vid_flash_api')[0].removeEventListener){
			$('#big-video-vid_flash_api')[0].removeEventListener('ended', novasImagens ,false);
			$('#big-video-vid').remove();
			if(typeof BV != 'undefined')
				delete BV;
		}		
	}
}

/* Abre Item do Menu - Nós */
function abreItemNos(){
	$('#main').attr('data-secao', 'nos');

	$.post(BASE+'ajax/nos', function(retorno){
		retorno = JSON.parse(retorno);
		
		var slides;
		var altura_util   = window.innerHeight;
		var largura_util  = window.innerWidth;
		var ratio         = largura_util / altura_util;
		var altura_texto  = window.innerHeight - parseInt($('nav ul li a.ativo').parent().find('.marcador-right').offset().top) - 1;
		var largura_texto = (parseInt($('#texto').css('width')) / 2) - 70;

		css = { 'height' : altura_util };
		
		retorno.imagens.map( function(self){
			slides += "<img src='"+self+"'>";
		});
		var texto = "<div class='texto-left animate'>"+retorno.titulo+"</div><div class='texto-right animate'>"+retorno.texto+"</div>";

		$('#menu-container').animate({ left : 0 }, 400, function(){
			$('#texto').html(texto).animate({
					height : altura_texto
			}, 300, function(){
				$('#texto').find('.texto-left, .texto-right').css('width', largura_texto);
				$('#texto').find('.animate').animate( { 'opacity' : 1 } );
				$('#bg_slides').html(slides).find('img').css(css).waitForImages( function(){
					$('#bg_slides').fadeIn('slow', TRANSICAO).cycle({
						'speed'   : 2000,
						'timeout' : 500
					});
				});
			});			
		});
	});
}

/* Fecha Item do Menu - Nós */
function fechaItemNos(){
	$('#bg_slides').fadeOut('slow');
	$('#texto').animate({ 'height': 0 }, 300);
}

/* Abre Item do Menu - Inspirações */
function abreItemInspiracoes(){
	$('#main').attr('data-secao', 'inspiracoes');
	$('#main').addClass('animando');

	var toShow = $('.insp-tile').filter(':hidden');
	
	if(toShow.length){

		var atual = toShow.filter(':first');
		var margem = 0;

		var visible_width = 0;
		$('.insp-tile:visible').each( function(){
			visible_width += parseInt($(this).css('width'));
		});
		var speed = 175;

		$('#menu-container').animate({ left : 0 }, 400, function(){

			toShow.filter(':first')
				  .css({
				  	'margin-left' : window.innerWidth - visible_width,
				  	'opacity'	  : 0.2,
				  	'display'	  : 'block'
				  });

			toShow.filter(':first')
				  .stop()
				  .animate({
				  	'margin-left' : 0,
				  	'opacity' 	  : 1
				  }, speed, TRANSICAO, function(){
						abreItemInspiracoes();
					});
		});
	}else{
		$('#main').removeClass('animando');
	}
}

/* Fecha Item do Menu - Inspirações */
function fechaItemInspiracoes(){
	$('.insp-tile').fadeOut('slow');
}

/* Abre Item da seção Inspirações - Celebrações */
function abreGaleriaCelebracoes(){

	$('#menu-container').animate({
		left : -1 * parseInt($('#menu-container').css('width'))
	}, 300);

	$('.insp-tile').fadeOut('slow');

	$.post(BASE+'ajax/abreGaleriaCelebracoes', function(retorno){
		
		var html;
		var altura_util   = window.innerHeight;
		var largura_util  = window.innerWidth;
		var ratio         = largura_util / altura_util;

		retorno = JSON.parse(retorno);

		retorno.map( function(self){
			html += "<img src='"+self+"'>";
		});
		
		css = { 'height' : altura_util };

		if(isiPad){
			efeito = 'scrollHorz';
			timeout = 16000;
		}else{
			efeito = 'fade';
			timeout = 4000;
		}
		
		$('#bg_slides').html(html).find('img').css(css);

		$('#bg_slides').fadeIn('slow', TRANSICAO).cycle({ 
			fx     : efeito,
			timeout: timeout,
			next   : $('#controle_slides .next'),
			prev   : $('#controle_slides .prev')
		});

		$('#bg_slides').on('swipeleft', function(){
			$('#bg_slides').cycle('next');
		});

		$('#bg_slides').on('swiperight', function(){
			$('#bg_slides').cycle('prev');
		});			

		$('#logo_link').fadeIn('slow');
		$('#controle_slides').fadeIn('slow');
	});
}

/* Abre Item da seção Inspirações - Expressões */
function abreGaleriaExpressoes(){

	$('#logo_link').fadeIn('slow');

	$('#menu-container').animate({
		left : -1 * parseInt($('#menu-container').css('width'))
	}, 300);

	$('.insp-tile').fadeOut('slow');

	$.post(BASE+'ajax/abreGaleriaExpressoes', function(retorno){
		
		var html;
		var altura_util   = window.innerHeight;
		var largura_util  = window.innerWidth;
		var ratio         = largura_util / altura_util;

		retorno = JSON.parse(retorno);

		retorno.map( function(self){
			html += "<img src='"+self+"'>";
		});
		
		css = { 'height' : altura_util };
		
		$('#bg_slides').html(html).find('img').css(css);

		if(isiPad){
			efeito = 'scrollHorz';
			timeout = 16000;
		}else{
			efeito = 'fade';
			timeout = 4000;
		}

		$('#bg_slides').fadeIn('slow', TRANSICAO).cycle({ 
			fx     : efeito,
			timeout: timeout,
			next   : $('#controle_slides .next'),
			prev   : $('#controle_slides .prev')
		});

		$('#bg_slides').on('swipeleft', function(){
			$('#bg_slides').cycle('next');
		});

		$('#bg_slides').on('swiperight', function(){
			$('#bg_slides').cycle('prev');
		});		
		
		$('#controle_slides').fadeIn('slow');
	});	
}


/* Fecha Item da seção Inspirações - Celebrações e Expressões */
function fechaGaleria(object){

	object.parent().fadeOut('slow');

	$('#menu-container').animate({
		left : 0
	}, 300, function(){
		$('.ativo').removeClass('ativo').trigger('mouseout', function(){});
		$('#bg_slides').fadeOut().unbind();
		$('#bg_slides').cycle('destroy');
		$('#controle_slides').fadeOut()
		$('#menu-inspiracoes').trigger('click').trigger('mouseover');
	});
}

/* Abre Item da seção Inspirações - Affair */
function abreAffair(){
	$('#main').attr('data-secao', 'affair');
	var altura_util   = window.innerHeight;
	var largura_util  = window.innerWidth;
	var ratio         = largura_util / altura_util;
	var altura_texto  = window.innerHeight - parseInt($('nav ul li a.ativo').parent().find('.marcador-right').offset().top) - 1;
	var slides        = "<img src='_imgs/layout/affair.jpg'>";

	css = { 'height' : altura_util };
	
	$('.insp-tile').fadeOut('slow');

	$('#bg_slides').html(slides).find('img').css(css).waitForImages( function(){
		$('#bg_slides').fadeIn('slow', TRANSICAO, function(){
			$('#texto-affair').animate({
					height : altura_texto
			}, 300, function(){
				$('#texto-affair').find('#affair-titulo').fadeIn('slow');
			});					
		})
	});
}

/* Fecha Item da seção Inspirações - Affair */
function fechaAffair(){
	$('#bg_slides').fadeOut('normal');
	$('#texto-affair').animate({ 'height' : 0 }, 300, TRANSICAO);
	$('#texto-affair').find('#affair-titulo').fadeOut('slow');
}

/* Abre o vídeo de Affair em FullScreen */
function mostraVideoAffair(){
	var altura_util   = window.innerHeight;
	var largura_util  = window.innerWidth;

	$('#video-affair').fadeIn('slow', TRANSICAO, function(){
		if(isiPad)
			var loading = $('<img id="playbtn" style="width:40px;" src="_imgs/layout/play.png">')
		else
			var loading = $("<div id='floatingCirclesG-Affair'><div class='f_circleG' id='frotateG_01'></div><div class='f_circleG' id='frotateG_02'></div><div class='f_circleG' id='frotateG_03'></div><div class='f_circleG' id='frotateG_04'></div><div class='f_circleG' id='frotateG_05'></div><div class='f_circleG' id='frotateG_06'></div><div class='f_circleG' id='frotateG_07'></div><div class='f_circleG' id='frotateG_08'></div></div>");

		var html = "<video id='affair' width='"+largura_util+"px' height='"+altura_util+"px'>";
		html += "<source src='http://player.vimeo.com/external/49263155.sd.mp4?s=19ad86880756a19620816170fa7795c6' type='video/mp4'>";
		html += "</video>";

		var ah = 60;
		var aw = 60;
		var ph = altura_util;
		var pw = largura_util;
		var mh = Math.ceil((ph-ah) / 2);
		
		var mw = Math.ceil((pw-aw) / 2);
		
		loading.css({
			top      : mh+'px',
			left     : mw+'px'
		});

		$(this).append(html).append(loading);
		$('#affair').attr('src', 'http://player.vimeo.com/external/49263155.sd.mp4?s=19ad86880756a19620816170fa7795c6');
		$('#affair')[0].load();

		if(isiPad)
			control = true;
		else
			control = false;

		_V_("affair", {
			autoplay : true,
			preload  : 'auto',
			loop     : false,
			width    : largura_util+'px',
			height   : altura_util+'px',
			controls : control
		}, function(){

			var playerAffair = this;
			if(isiPad){
				$('#playbtn').bind('click', function(){
					loading.remove();
					_V_("affair").play();
				});
			}else{
				loading.fadeOut(8000, function(){
					$(this).remove();
				});	
			}
			

      		/* Botão para Fechar o vídeo de Affair */
			$('#video-affair a').live('click', function(e){

				e.preventDefault();
				playerAffair.pause();
				if(playerAffair.techName == "html5"){        
				    playerAffair.tag.src = "";                 
				    playerAffair.tech.removeTriggers();        
					playerAffair.load();                       
				}   
				playerAffair.tech.destroy();
				playerAffair.destroy();                      
				$(playerAffair.el).remove();
				$('#video-affair').fadeOut('slow', TRANSICAO, function(){
					delete($('#video-affair video')[0]);
					$('#floatingCirclesG-Affair').remove();
					if($('#video-affair video').length)
						$('#video-affair video').remove();
				});

			})
    	});
	});
}

var requestState = 0;

/* Abre Item do Menu - Algumas Palavras */
function abreItemPalavras(self){
	$('#main').attr('data-secao', 'palavras');
	try{
		id = self.attr('data-next');
	}catch(e){
		id = 'land';
	}

	if(requestState == 0){

		requestState = 1;

		$.post(BASE+'ajax/depoimentos', { id : id }, function(retorno){

			retorno = JSON.parse(retorno);
			
			var slides        = "";
			var altura_util   = window.innerHeight;
			var largura_util  = window.innerWidth;
			var ratio         = largura_util / altura_util;
			var altura_texto  = window.innerHeight - parseInt($('nav ul li a.ativo').parent().find('.marcador-right').offset().top) - 1;
			var largura_texto = (parseInt($('#texto').css('width')) / 2) - 60;

			if(ratio < 1.8)
				css = { 'height' : altura_util };
			else
				css = { 'width' : largura_util };

			retorno.imagens.map( function(self){
				slides += "<img src='"+self+"'>";
			});
			
			if(parseInt($('#texto-depoimentos').css('height')) == 0){

				$('#texto-depoimentos').fadeIn('normal', function(){

					$('#texto-depoimentos').animate({
							height : altura_texto
					}, 300, function(){

						$('#texto-depoimentos').find('.tit').html(retorno.titulo).fadeIn();
						$('#texto-depoimentos').find('.dep').html(retorno.texto).fadeIn();
						$('#dep-start').fadeIn();

						$('#bg_slides').fadeOut('normal').html(slides).find('img').css(css).waitForImages( function(){

							$('#bg_slides').fadeIn('slow', TRANSICAO, function(){
								requestState = 0;
							});

						});
					});

				});

			}else{

				$('#dep-start').fadeOut();

				$('#texto-depoimentos').find('.tit').fadeOut('normal', function(){
					$(this).html(retorno.titulo);
				});

				$('#texto-depoimentos').find('.dep').fadeOut('normal', function(){
					$(this).html(retorno.texto);
				});

				$('#bg_slides').prepend(slides).find('img').css(css).waitForImages( function(){

					$('#bg_slides img').filter(':last').fadeOut(1000, TRANSICAO, function(){

						$('#bg_slides img').filter(':last').remove();

						if($('#bg_slides img').length > 1)
							$('#bg_slides').cycle();
						
						$('#texto-depoimentos').find('.tit').fadeIn();
						$('#texto-depoimentos').find('.dep').fadeIn();

						if(id == 'land'){
							$('#dep-nav').fadeOut();
							$('#dep-start').fadeIn();
						}else{
							$('#dep-nav').fadeIn();
							$('#dep-start').fadeOut();
						}

						requestState = 0;

					})
				});

				$('#dep-nav').fadeIn('slow', function(){
					$('#dep-prev').attr('data-next', retorno.prev);
					$('#dep-next').attr('data-next', retorno.next);	
				});
			}
		});
	}
}

/* Fecha Item do Menu - Algumas Palavras */
function fechaItemPalavras(){
	$('#bg_slides').fadeOut('slow');
	$('#texto-depoimentos').find('.tit').html('');
	$('#texto-depoimentos').find('.dep').html('');
	$('#dep-start').fadeOut();
	$('#dep-nav').fadeOut();
	$('#texto-depoimentos').animate({ 'height': 0 }, 300);
}

/* Abre Item do Menu - Contato */
function abreItemContato(){
	$('#main').attr('data-secao', 'contato');
	$('#main').addClass('animando');

	var toShow = $('.tile-contato img').filter(':hidden');

	if(toShow.length){

		var atual = toShow.filter(':first');
		var margem = 0;
		var speed = 600;

		atual.parent().css({ 'display' : 'block' });

		atual.css({
			  	'margin-left' : atual.css('width'),
			  	'display' 	  : 'block',
			  	'opacity'	  : 0
			  })
			  .animate({
			  	'margin-left' : 0,
			  	'opacity'	  : 1
			  }, speed, TRANSICAO, function(){
					setTimeout('abreItemContato();', 15);
			  });

	}else{

		var altura_texto  = parseInt($('nav ul li a.ativo').parent().find('.marcador-right').offset().top) - 30;
		
		$('#info-contato').css({
			'top'  : altura_texto,
			'left'    : '90%',
			'opacity' : 0,
			'display' : 'block'
		}).animate({
			'left'    : '25%',
			'opacity' : 1
		}, 600, function(){
			$('#main').removeClass('animando');
		});
	}	
}

/* Fecha Item do Menu - Contato */
function fechaItemContato(){
	$('#info-contato').fadeOut('slow', function(){
		$('.tile-contato img').fadeOut('slow', function(){
			$('.tile-contato').css({ 'display' : 'none' })		
		});
	});
}

function fechaInicial(){
	if($('.tile:visible').length)
		$('.tile').fadeOut('slow');

	if($('#dog-play').length && $('#dog-play').is(':visible'))
		$('#dog-play').fadeOut();

	$('#main').removeClass('inicial');
}