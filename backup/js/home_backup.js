$('document').ready( function(){

	if(window.innerHeight < 700 && $.browser.msie){
		$('html body #main #menu-container nav ul').css('margin-top', '30%');
	}
		

	/* Muta o som do vídeo de background de acordo com o hash */
	// if(window.location.hash == 'nosound' || window.location.hash == '#nosound'){
	// 	$('#video video')[0].muted = true;
	// }
	
	
	/*
	 * Comportamento dos itens do menu 
	 * Ao clicar em um item, é necessário fechar o item atual e abrir a animação
	 * TODO: Executar a função para fechar somente para o item que estiver aberto
	  		 utilizando um marcador na div #main (podendo assim controlar os links do menu
	  		 	para não abrirem se já estiverem abertos DUH)
	*/
	$('.menu-link').live('click', function(e){
		e.preventDefault();
		
		//paraAnimacaoHome();
		switch($('#main').attr('data-secao')){
			case 'inicial':
				fechaInicial();
				break;
			case 'nos':
				fechaItemNos();
				break;
			case 'inspiracoes':
				fechaItemInspiracoes();
				break;
			case 'palavras':
				fechaItemPalavras();
				break;
			case 'contato':
				fechaItemContato();
				break;
			case 'affair':
				fechaAffair();
				break;
		}

		$('.ativo').removeClass('ativo').trigger('mouseout', function(){});
		$(this).addClass('ativo');

		switch($(this).attr('id')){
			case 'menu-inicio':
				abreVideoInicio();
				break;
			case 'menu-nos':
				abreItemNos();
				break;
			case 'menu-inspiracoes':
				abreItemInspiracoes();
				break;
			case 'menu-palavras':
				abreItemPalavras();
				break;
			case 'menu-contato':
				abreItemContato();
				break;
		}		
	});

	/* Animação dos marcadores dos itens do menu */
	$('nav li a').hover( function(){

		largura_menu = parseInt($('#menu-container').css('width')) / 2;
		largura_link = parseInt($(this).css('width')) / 2;
		largura = largura_menu - largura_link - 9;
		
		$($(this).parent().find('.marcador-left, .marcador-right')).stop().animate({
			width   : largura,
			opacity : 1
		}, 300);
		
	}, function(){

		if(!$(this).hasClass('ativo')){
			$($(this).parent().find('.marcador-left, .marcador-right')).stop().animate({
				width   : 0,
				opacity : 0
			}, 300);
		}

	});

	/* Centraliza verticalmente a mensagem de 'Carregando...' e faz o preload das imagens */
	$('#Vcenter').vAlign().fadeIn(1500, TRANSICAO, function(){

		imgList = [
			/* Imagens de Tile 20% da Home */
			BASE+'_imgs/layout/home1.jpg',
			BASE+'_imgs/layout/home2.jpg',
			BASE+'_imgs/layout/home3.jpg',
			BASE+'_imgs/layout/home4.jpg',
			/* Imagens de Poster dos Vídeos */
			BASE+'_imgs/inicial-poster.jpg',
			BASE+'_imgs/layout/play.png',
			/* Imagem de fundo de Nós */
			BASE+'_imgs/slides/slide1.jpg',
			/* Imagens de Tile de Inspirações*/
			BASE+'_imgs/layout/inspiracoes1.jpg',
			BASE+'_imgs/layout/inspiracoes2.jpg',
			BASE+'_imgs/layout/inspiracoes3.jpg',
			/* Imagem de fundo de Affair */
			BASE+'_imgs/layout/affair.jpg',
			/* Imagens de Tile de Contato */
			BASE+'_imgs/layout/contato1.jpg',
			BASE+'_imgs/layout/contato2.jpg',
			/* Imagens de navegação e logo */
			BASE+'_imgs/layout/marca.png',
			BASE+'_imgs/layout/next.png',
			BASE+'_imgs/layout/next-arrow.png',
			BASE+'_imgs/layout/plus-sign.png',
			BASE+'_imgs/layout/prev.png',
			BASE+'_imgs/layout/prev-arrow.png',
			/* Imagens de Depoimentos*/
			BASE+'_imgs/depoimentos/capa.jpg',
			BASE+'_imgs/depoimentos/juliana_ricardo.jpg',
			BASE+'_imgs/depoimentos/bebel_werner.jpg',
			BASE+'_imgs/depoimentos/bruna_fabiano.jpg',
			BASE+'_imgs/depoimentos/camila_fabio.jpg',
			BASE+'_imgs/depoimentos/daniela_joao_gustavo.jpg',
			BASE+'_imgs/depoimentos/danielle_caio.jpg',
			BASE+'_imgs/depoimentos/fabiana_gustavo.jpg',
			BASE+'_imgs/depoimentos/fernanda_thiago.jpg',
			BASE+'_imgs/depoimentos/giselle_jeremy.jpg',
			BASE+'_imgs/depoimentos/isabelle_pedro.jpg',
			BASE+'_imgs/depoimentos/juliana_andre.jpg',			
	        BASE+'_imgs/depoimentos/karina_bernardo.jpg',
	        BASE+'_imgs/depoimentos/lia_fernando.jpg',
	        BASE+'_imgs/depoimentos/mariana_francisco.jpg',
	        BASE+'_imgs/depoimentos/paula_ricardo.jpg',
	        BASE+'_imgs/depoimentos/regina_tiago.jpg',
	        //BASE+'_imgs/depoimentos/fernanda_rodrigo.jpg',
	        //BASE+'_imgs/depoimentos/simone_fabio.jpg',
	        //BASE+'_imgs/depoimentos/graziela_claudio.jpg',
	        //BASE+'_imgs/depoimentos/ana_beatriz_fernando.jpg',
	        //BASE+'_imgs/depoimentos/beatriz_andre.jpg',
	        /* Primeiras Imagens das Galeria */
	        BASE+'_imgs/galerias/07900001.jpg',
			BASE+'_imgs/galerias/celebracoes/000085.jpg'
		];
		
		$.preLoadImages(
			imgList, function(){
				iniciaAnimacaoHome();
		});
	});

	/* Comportamento dos links da seção Inspirações */
	$('.insp-tile a').live('click', function(e){
		e.preventDefault();
		if(!$('#main').hasClass('animando')){
			switch($(this).attr('id')){
				case 'celebracoes-link':
					abreGaleriaCelebracoes();
					break;
				case 'expressoes-link':
					abreGaleriaExpressoes();
					break;
				case 'affair-link':
					abreAffair();
					break;
			}
		}
	});

	/* Botão de voltar das galeria de imagem full screen */
	$('#logo_link a').live('click', function(e){
		e.preventDefault();
		fechaGaleria($(this));
	});

	/* Botão para fechar o vídeo Inicial */
	$('#logo_link_inicial a').live('click', function(e){
		e.preventDefault();
		fechaVideoInicio();
	});	

	/* Navegação dos depoimentos */
	$('#dep-nav a, #dep-start').live('click', function(e){
		e.preventDefault();
		abreItemPalavras($(this));
	});

	/* Botão para Iniciar o vídeo de Affair */
	$('#start-affair').live('click', function(e){
		e.preventDefault();
		mostraVideoAffair();
	});

});