$('document').ready( function(){

	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	if(isiPad && Math.abs(window.orientation) != 90){
		window.location = 'http://www.aqfl.com.br/mobile/home';
	}

	$(window).bind( 'orientationchange', function(e){
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		if(isiPad && Math.abs(window.orientation) != 90){
			window.location = 'http://www.aqfl.com.br/mobile/home';
		}
	});

});