function multiply(topValue, bottomValue){
	return topValue * bottomValue / 255;
}

$('document').ready( function(){

	var multiplyColor = [22, 34, 44];

	var canvas_topo = $('#fundo-azul')[0];
	var contexto_topo = canvas_topo.getContext('2d');
	var container_topo = $('#wrapper');
	contexto_topo.canvas.width  = 550;
	contexto_topo.canvas.height = window.innerHeight;
	contexto_topo.fillStyle = '#16222C';
	contexto_topo.fillRect(0,0,550,parseInt(container_topo.css('height')));

	var canvas_fundo = $('#backdrop')[0];	
	var contexto_fundo = canvas_fundo.getContext('2d');
	contexto_fundo.canvas.width  = window.innerWidth;
	contexto_fundo.canvas.height = window.innerHeight;

    var pattern_img = new Image();
    pattern_img.onload = function(){

		var pattern = contexto_fundo.createPattern(pattern_img, "repeat");

		contexto_fundo.fillStyle = pattern;
		contexto_fundo.fillRect(0,0,contexto_fundo.canvas.width,contexto_fundo.canvas.height);
		contexto_fundo.blendOnto(contexto_topo, 'multiply');
	};
	pattern_img.src = "_imgs/layout/pattern.png";

});