$.fn.vAlign = function() {
	return this.each(function(i){
	var ah = $(this).height();
	var ph = $(this).parent().height();
	var mh = Math.ceil((ph-ah) / 2);
	$(this).css('margin-top', mh);
	});
};

var imgs_first_set = [
	BASE+'_imgs/layout/home2.jpg',
	BASE+'_imgs/layout/home1.jpg',
	BASE+'_imgs/layout/home3.jpg',
	BASE+'_imgs/layout/home4.jpg'
];
var imgs_second_set = [
	BASE+'_imgs/layout/home5.jpg',
	BASE+'_imgs/layout/home6.jpg',
	BASE+'_imgs/layout/home7.jpg',
	BASE+'_imgs/layout/home8.jpg'
];

function mostrarImagens(){
	
	var toShow = $('.tile').filter(':hidden');

	if(toShow.length){

		var atual = toShow.filter(':first');
		var margem = 0;

		if(atual.hasClass('canv')){

			var multiplyColor = [22, 34, 44];

			var canvas_fundo = $('#tile-canvas')[0];
			var ctx_fundo = canvas_fundo.getContext('2d');

			var canvas_menu = $('#menu-canvas')[0];
			var ctx_menu = canvas_menu.getContext('2d');
			ctx_menu.clearRect(0,0,ctx_menu.canvas.width,ctx_menu.canvas.height);
			ctx_menu.fillStyle = '#16222C';
			ctx_menu.fillRect(0,0,ctx_menu.canvas.width,ctx_menu.canvas.height);

			var bg_img = new Image();
		    bg_img.onload = function(){
		    	ctx_fundo.drawImage(bg_img, 0, 0);
				ctx_fundo.blendOnto(ctx_menu, 'multiply');
			};
			
			bg_img.src = atual.attr('imgfundo');
			
			toShow.filter(':first').animate({ 'margin-top' : margem }, 600, 'easeInExpo', function(){
				$(this).fadeIn(150, function(){
					setTimeout('mostrarImagens();', 1000);
				});
			});	

		}else{
			toShow.filter(':first').animate({ 'margin-top' : margem }, 600, 'easeInExpo', function(){
				$(this).fadeIn(150, function(){
					setTimeout('mostrarImagens();', 1000);
				});
			});	
		}

	}else{
		setTimeout('escondeImagens(1);', 1000);
		$('#bg_video')[0].currentTime = 0;
		$('#bg_video')[0].play();
	}
}

function escondeImagens(exec){
	var toHide = $('.tile').filter(':visible');
	if(toHide.length){

		var atual = toHide.filter(':last');
		var multiplicador = (exec % 2 == 0) ? -1 : 1;
		var margem = parseInt(atual.css('height')) * multiplicador;

		if(atual.hasClass('canv')){

			var canvas_menu = $('#menu-canvas')[0];
			var ctx_menu = canvas_menu.getContext('2d');
			ctx_menu.clearRect(0,0,ctx_menu.canvas.width,ctx_menu.canvas.height);
			ctx_menu.fillStyle = 'rgba(22,34,44,.90)';
			ctx_menu.fillRect(0,0,ctx_menu.canvas.width,ctx_menu.canvas.height);

			toHide.filter(':last').animate({ 'margin-top' : margem }, 600, 'easeInExpo', function(){
				$(this).hide(0, function(){
					setTimeout('escondeImagens('+(exec+1)+');', 1000);	
					if(!$('.tile').filter(':visible').length)
						$('#menu-container').delay(150).animate({ left : 0 }, 400);
				});
			});	

		}else{
			toHide.filter(':last').animate({ 'margin-top' : margem }, 600, 'easeInExpo', function(){
				$(this).hide(0, function(){
					setTimeout('escondeImagens('+(exec+1)+');', 1000);	
					if(!$('.tile').filter(':visible').length)
						$('#menu-container').delay(150).animate({ left : 0 }, 400);
				});
			});			
		}
	}
};

function maximizaVideo(){
	var video        = $('#video video');
	var altura_util  = window.innerHeight;
	var largura_util = window.innerWidth;
	var ratio;

	ratio = largura_util / altura_util;
	if(ratio < 1.8)
		video.height = altura_util;
	else
		video.width = largura_util;
};

function trocaImagens(img_set){

	i=0;
	$('.tile').each( function(){
		if($(this).hasClass('canv')){
			$(this).attr('imgfundo', img_set[i]);
		}else{
			$(this).attr('src', img_set[i]);
		}
		i++;
	});
}

function novasImagens(e){
	if(!e) { e = window.event; }
	var video = $('#bg_video');
	var mostrar;

	if(video.hasClass('first_set')){
		video.removeClass('first_set').addClass('second_set');
		mostrar = imgs_second_set;
	}else{
		video.addClass('first_set').removeClass('second_set');
		mostrar = imgs_first_set;
	}
	
	if($('.tile:nth-child(2)').hasClass('canv')){
		$('#tile-first').remove();
		var clone = "<img class='tile tile-image' id='tile-first' src='_imgs/layout/home1.jpg'>"
		$('#tile-canvas').after(clone);
	}

	trocaImagens(mostrar);
	mostrarImagens();
}


$('document').ready( function(){

	$('#Vcenter').vAlign().fadeIn(1500, 'easeInExpo', function(){

		$('#main').waitForImages( function(){

			maximizaVideo();

			var multiplyColor = [22, 34, 44];

			var canvas_fundo = $('#tile-canvas')[0];
			var ctx_fundo = canvas_fundo.getContext('2d');
			ctx_fundo.canvas.width  = parseInt($('.tile-image:first').css('width')) * 2;
			ctx_fundo.canvas.height = window.innerHeight;

			var canvas_menu = $('#menu-canvas')[0];
			var ctx_menu = canvas_menu.getContext('2d');
			ctx_menu.canvas.width  = parseInt($('.tile-image:first').css('width'));
			ctx_menu.canvas.height = window.innerHeight;
			ctx_menu.fillStyle = '#16222C';
			ctx_menu.fillRect(0,0,ctx_menu.canvas.width,ctx_menu.canvas.height);

			var bg_img = new Image();
		    bg_img.onload = function(){
		    	ctx_fundo.drawImage(bg_img, 0, 0);
				ctx_fundo.blendOnto(ctx_menu, 'multiply');

				$('#bg_video')[0].play();
				
				$('#loading').delay(300).fadeOut(1800, 'easeOutExpo', function(){
					escondeImagens(1);
				});
			};
			bg_img.src = "_imgs/layout/home2.jpg";
		});

	});
	
	$.preLoadImages(imgs_second_set, function(){
		$('#bg_video')[0].addEventListener('ended', novasImagens ,false);	
	});

});