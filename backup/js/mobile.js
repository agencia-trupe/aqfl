$('document').ready( function(){

	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	if(isiPad && Math.abs(window.orientation) == 90){
		window.location = 'http://www.aqfl.com.br/';
	}
	
	$(window).bind( 'orientationchange', function(e){
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		if(isiPad && Math.abs(window.orientation) == 90){
			window.location = 'http://www.aqfl.com.br/';
		}
	});

	var larguraTela = window.innerWidth;
	var alturaTela = window.innerHeight;

	$('nav ul li a').each( function(){
		if(parseInt($(this).css('height')) > 45){
			$(this).css('line-height', '25px')
		}
	});

	$('.main img').each( function(){
		$(this).css('max-width', larguraTela);
	});

	if($('#video').length){
		var player = _V_("video");
		if(larguraTela < 760){
			player.size(larguraTela, (larguraTela / 1.7));
		}else{
			player.size(768, (768 / 1.7));
		}
	}

	if($('.texto-celebracoes').length){
		$('.texto-celebracoes').cycle({
			next : $('#next'),
			prev : $('#prev'),
			fx : 'scrollHorz'
		});

		$('.texto-celebracoes').on('swipeleft', function(){
			$('.texto-celebracoes').cycle('next');
		});

		$('.texto-celebracoes').on('swiperight', function(){
			$('.texto-celebracoes').cycle('prev');
		});					
	}

	if($('.texto-expressoes').length){
		$('.texto-expressoes').cycle({
			next : $('#next'),
			prev : $('#prev'),
			fx : 'scrollHorz'
		});

		$('.texto-expressoes').on('swipeleft', function(){
			$('.texto-expressoes').cycle('next');
		});

		$('.texto-expressoes').on('swiperight', function(){
			$('.texto-expressoes').cycle('prev');
		});			
	}

	$(window).resize( function(){
		larguraTela = window.innerWidth;
		alturaTela = window.innerHeight;
		$('img').each('css', larguraTela);
	})

});