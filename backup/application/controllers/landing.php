<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller {

    function __construct(){
   		parent::__construct();

      	$this->load->library('user_agent');
	    if($this->agent->is_mobile() || ($this->agent->browser() == 'Internet Explorer' && $this->agent->version() <= (int) 8)){

            if($this->agent->browser() == 'Internet Explorer' && $this->agent->version() <= (int) 6){
                redirect('ie8');
            }

	        $is_iPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');

	        if (!$is_iPad)
	            redirect('mobile/home');
	    }
	}

    function index(){
   		$this->load->view('landing');
    }

}