<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');
   }

    function nos(){

    	echo json_encode(array(

    		'imagens' => array(
				    		'_imgs/slides/slide1.jpg'
    					),

    		'titulo' => "<span class='f-42'>Lente, ângulo, luz e ambiente</span><br>são capazes de tornar uma imagem mais bela...<br>MAS <span class='f-42'>NENHUM DELES</span> TEM A FORÇA<br>DE REVELAR <span class='f-42'>SENTIMENTO</span>.",

    		'texto'  => 'Imagens vivas precisam de um fotógrafo que sinta, muito mais do que veja. Tem que gostar de gente e de se envolver de verdade com suas histórias. Nós, Anna e Fabio, definimos assim nossa fotografia, e não temos dúvida de que essa sintonia de pensamentos e propósitos foi o que nos aproximou e uniu nossos trabalhos. Um projeto que só tem sentido porque é feito de pessoas apaixonadas pelo que fazem para pessoas e histórias apaixonantes. E é por isso que, mais do que esperar, desejamos sua visita.  Venha tomar um café conosco. Estamos ansiosos para ouvir suas histórias.'

    	));

    }

    function abreGaleriaCelebracoes(){

      echo json_encode(array(
        '_imgs/galerias/celebracoes/000085.jpg',
        '_imgs/galerias/celebracoes/000128-2.jpg',
        '_imgs/galerias/celebracoes/0049-3.jpg',
        '_imgs/galerias/celebracoes/0127.jpg',
        '_imgs/galerias/celebracoes/0214.jpg',
        '_imgs/galerias/celebracoes/0328.jpg',
        '_imgs/galerias/celebracoes/0479.jpg',
        '_imgs/galerias/celebracoes/0640.jpg',
        '_imgs/galerias/celebracoes/0821.jpg',
        '_imgs/galerias/celebracoes/0834.jpg',
        '_imgs/galerias/celebracoes/1116.jpg',
        '_imgs/galerias/celebracoes/23-3.jpg',
        '_imgs/galerias/celebracoes/AnnaQuast-002.jpg',
        '_imgs/galerias/celebracoes/AnnaQuast-003.jpg',
        '_imgs/galerias/celebracoes/AnnaQuast-006.jpg',
        '_imgs/galerias/celebracoes/AnnaQuast-007.jpg',
        '_imgs/galerias/celebracoes/AnnaQuast-012.jpg',
        '_imgs/galerias/celebracoes/AnnaQuast-013.jpg',
        '_imgs/galerias/celebracoes/AQ00020017-2.jpg',
        '_imgs/galerias/celebracoes/AQ00122743-2.jpg',
        '_imgs/galerias/celebracoes/AQ00130560-2.jpg',
        '_imgs/galerias/celebracoes/AQ00180304-2.jpg',
        '_imgs/galerias/celebracoes/AQ00200945.jpg',
        '_imgs/galerias/celebracoes/AQ00201115.jpg',
        '_imgs/galerias/celebracoes/AQ00250205-137.jpg',
        '_imgs/galerias/celebracoes/AQ00251502-815.jpg',
        '_imgs/galerias/celebracoes/AQ00280903-2.jpg',
        '_imgs/galerias/celebracoes/AQ00301367-2-2.jpg',
        '_imgs/galerias/celebracoes/AQ00401980.jpg',
        '_imgs/galerias/celebracoes/AQ00402033.jpg',
        '_imgs/galerias/celebracoes/AQ00402052.jpg',
        '_imgs/galerias/celebracoes/AQ00540699-2.jpg',
        '_imgs/galerias/celebracoes/AQ00542274.jpg',
        '_imgs/galerias/celebracoes/AQ00542604.jpg',
        '_imgs/galerias/celebracoes/AQ00542680.jpg',
        '_imgs/galerias/celebracoes/AQ00640485.jpg',
        '_imgs/galerias/celebracoes/AQ00650206-2.jpg',
        '_imgs/galerias/celebracoes/AQ00680822-2.jpg',
        '_imgs/galerias/celebracoes/AQ00681089-2.jpg',
        '_imgs/galerias/celebracoes/AQ00760554-2.jpg',
        '_imgs/galerias/celebracoes/Barmitsva1-2.jpg',
        '_imgs/galerias/celebracoes/capa-33X44.jpg',
        '_imgs/galerias/celebracoes/FL00190156.jpg',
        '_imgs/galerias/celebracoes/FL0050121-2.jpg',
        '_imgs/galerias/celebracoes/FL0116 0309.jpg',
        '_imgs/galerias/celebracoes/FL01230266.jpg',
        '_imgs/galerias/celebracoes/FL01700124.jpg',
        '_imgs/galerias/celebracoes/FL02360536 copy.jpg',
        '_imgs/galerias/celebracoes/FL02840440.jpg',
        '_imgs/galerias/celebracoes/FL02850996.jpg',
        '_imgs/galerias/celebracoes/FL02900785.jpg',
        '_imgs/galerias/celebracoes/FL03130350.jpg',
        '_imgs/galerias/celebracoes/FL03270357.jpg',
        '_imgs/galerias/celebracoes/FL03620624-2-2.jpg',
        '_imgs/galerias/celebracoes/FL03900311.jpg',
        '_imgs/galerias/celebracoes/FL03990833.jpg',
        '_imgs/galerias/celebracoes/FL03991828.jpg',
        '_imgs/galerias/celebracoes/FL04130045.jpg',
        '_imgs/galerias/celebracoes/FL04330215.jpg',
        '_imgs/galerias/celebracoes/FL04330740.jpg',
        '_imgs/galerias/celebracoes/FL04470194.jpg',
        '_imgs/galerias/celebracoes/FL04500024.jpg',
        '_imgs/galerias/celebracoes/FL04540275.jpg',
        '_imgs/galerias/celebracoes/FL04540401.jpg',
        '_imgs/galerias/celebracoes/FL04541024.jpg',
        '_imgs/galerias/celebracoes/FL04610487.jpg',
        '_imgs/galerias/celebracoes/FL04701602.jpg',
        '_imgs/galerias/celebracoes/FL04780232.jpg',
        '_imgs/galerias/celebracoes/FL05300303.jpg',
        '_imgs/galerias/celebracoes/FL05330149.jpg',
        '_imgs/galerias/celebracoes/FL05350568.jpg',
        '_imgs/galerias/celebracoes/FL05370262.jpg',
        '_imgs/galerias/celebracoes/FL05421110.jpg',
        '_imgs/galerias/celebracoes/FL05421174.jpg',
        '_imgs/galerias/celebracoes/FL05450811.jpg',
        '_imgs/galerias/celebracoes/FL05451230-2.jpg',
        '_imgs/galerias/celebracoes/FL06122217.jpg',
        '_imgs/galerias/celebracoes/FL06220292.jpg',
        '_imgs/galerias/celebracoes/FL06220456.jpg',
        '_imgs/galerias/celebracoes/FL06311233.jpg',
        '_imgs/galerias/celebracoes/FL06330623.jpg',
        '_imgs/galerias/celebracoes/FL06330686.jpg',
        '_imgs/galerias/celebracoes/FL06340596.jpg',
        '_imgs/galerias/celebracoes/FL06550382.jpg',
        '_imgs/galerias/celebracoes/FL06550637.jpg',
        '_imgs/galerias/celebracoes/FL06560407-2.jpg',
        '_imgs/galerias/celebracoes/FL06560957-2.jpg',
        '_imgs/galerias/celebracoes/FlaviaGalli_0022.jpg',
        '_imgs/galerias/celebracoes/LQ00030931-2.jpg',
        '_imgs/galerias/celebracoes/LQ00031597.jpg',
        '_imgs/galerias/celebracoes/LQ00080533.jpg',
        '_imgs/galerias/celebracoes/LQ00080733.jpg',
        '_imgs/galerias/celebracoes/LQ00080898.jpg',
        '_imgs/galerias/celebracoes/LQ00080999.jpg',
        '_imgs/galerias/celebracoes/pag_6-3.jpg',
        '_imgs/galerias/celebracoes/XFL410-0291.jpg'
      ));

    }

    function abreGaleriaExpressoes(){

      echo json_encode(array(
        '_imgs/galerias/07900001.jpg',
        '_imgs/galerias/AQ00470314.jpg',
        '_imgs/galerias/AQ00470475.jpg',
        '_imgs/galerias/AQ00500042-2.jpg',
        '_imgs/galerias/AQ00500159.jpg',
        '_imgs/galerias/AQ0058PARTE30130-2.jpg',
        '_imgs/galerias/AQ00700081-2.jpg',
        '_imgs/galerias/CamilaeGui0490.jpg',
        '_imgs/galerias/FL03580471.jpg',
        '_imgs/galerias/FL04180456.jpg',
        '_imgs/galerias/FL06031081.jpg',
        '_imgs/galerias/FL06250005.jpg',
        '_imgs/galerias/FL06260387.jpg',
        '_imgs/galerias/FL06520216.jpg',
        '_imgs/galerias/LQ00010095-2.jpg',
        '_imgs/galerias/LQ00010186.jpg',
        '_imgs/galerias/nina-10.jpg',
        '_imgs/galerias/rods-5.jpg',
        '_imgs/galerias/ED69105B.jpg'
      ));

    }

    function depoimentos(){

      $id = $this->input->post('id');

      $deps = array(

        array(
          'prev' => 'land',
          'data' => '07/11/2012',
          'imagens' => array('_imgs/depoimentos/juliana_ricardo.jpg'),
          'titulo' => "Juliana e Ricardo",
          'texto'  => "Estamos encantados com absolutamente tudo o que envolveu o trabalho! A equipe é fantástica (foram incríveis do começo ao fim), o álbum das provas é tão caprichado que até parece o final, as fotos ficaram maravilhosas e vocês foram sensacionais o tempo todo. Ficamos muito felizes com o carinho e bom humor constante que todos vocês estavam no dia mais especial das nossas vidas! Obrigada por toda a peciência no making of (até assistente do cabeleireiro vocês conseguiram ser! rsrs) e durante a festa para fazer as fotos que a gente inventou!! Ainda não conseguimos parar de ver as fotos, vai ser muito difícil escolher só algumas para o álbum!!! Muito obrigada e parabéns pelo trabalho tão lindo e pela equipe tão querida!",
          'next' => 1
        ),

        array(
          'prev' => 0,
          'data' => '05/11/2011',
          'imagens' => array('_imgs/depoimentos/karina_bernardo.jpg'),
          'titulo' => "KARINA E BERNARDO",
          'texto'  => 'Muuuuuuuito obrigada por tudo! Vocês foram anjos pra mim no dia do casamento! Fizeram parte daquele dia como poucas pessoas fizeram! Palavras não são suficientes para agradecer por tudo! Obrigada mesmo, de coração! Muitos beijos',
          'next' => 2
        ),

        array(
          'prev' => 1,
          'data' => '21/05/2011',
          'imagens' => array('_imgs/depoimentos/fernanda_thiago.jpg'),
          'titulo' => "FERNANDA E THIAGO",
          'texto'  => 'Tiramos uns minutos para agradecer a vocês que estiveram ao nosso lado durante o nosso casamento religioso, um dia tão especial. Estamos aguardando o dia das provas das fotos e do vídeo com muita ansiedade!!! Obrigado por tudo!!',
          'next' => 3
        ),

        array(
          'prev' => 2,
          'data' => '21/10/2010',
          'imagens' => array('_imgs/depoimentos/giselle_jeremy.jpg'),
          'titulo' => "GISELLE E JEREMY ",
          'texto'  => 'Seu profissionalismo, talento e trabalho são realmente incríveis, sentimos muito à vontade e, acima de tudo, muita empatia. Ficamos muito felizes com a presença de vocês nesse momento tão importante para nós! Muito muito obrigada por tudo! Desejo a vocês muito sucesso, que cresçam e brilhem ainda mais... vocês merecem! Se vierem a San Francisco, dêm um alô para nós! Beijos a todos e feliz ano novo!',
          'next' => 4
        ),

        array(
          'prev' => 3,
          'data' => '02/10/2010',
          'imagens' => array('_imgs/depoimentos/fabiana_gustavo.jpg'),
          'titulo' => "FABIANA E GUSTAVO ",
          'texto'  => 'Depois do casamento hoje foi um dia de grande emoção, pude rever esses lindos momentos com "os seus olhos" nas suas lindas fotografias!!!! Obrigada por eternizar tão bem esse momento tão especial para nós: Nosso CASAMENTO!!!! Estamos todos tão emocionados como no dia, revendo cada momento, sentindo tudo novamente porque vocês podem traduzir em imagens nossa felicidade nossa EMOÇÃO!!!! Obrigada pelo brilhante trabalho, pelo profissionalismo, (...) Obrigada por mostrar meu lado mais belo, as lágrimas de amor do Gu, minha homenagem ao meu pai... obrigada por estarem lá ao nosso lado! Seremos eternamente gratos! PARABÉNS PELO MARAVILHOSO TRABALHO!',  
          'next' => 5

        ),        

        array(
          'prev' => 4,
          'data' => '17/07/2010',
          'imagens' => array('_imgs/depoimentos/juliana_andre.jpg'),
          'titulo' => "JULIANA E ANDRÉ",
          'texto'  => 'André e eu gostaríamos de agradecer, mesmo antes de ver as fotos, pelo trabalho. Muito obrigada! Além de fotógrafos maravilhosos, vocês também foram maquiadores, psicólogos, costureiros, e fizeram absolutamente tudo para que a noiva neurótica se sentisse bem! Mal podemos esperar para ver as fotos! Beijos do casal!',
          'next' => 6
        ),

        array(
          'prev' => 5,
          'data' => '29/05/2010',
          'imagens' => array('_imgs/depoimentos/mariana_francisco.jpg'),
          'titulo' => "MARIANA E FRANCISCO",
          'texto'  => 'Queremos agradecer pelo enorme carinho, dedicação, profissionalismo e simpatia. Vocês foram o máximo, FIZERAM TODA A DIFERENÇA!!! E com certeza não só as fotos vão ficar pra sempre mas, vocês também. Beijos. Mariana, Chico e Cris',
          'next' => 7
        ),

        array(
          'prev' => 6,
          'data' => '15/05/2010',
          'imagens' => array('_imgs/depoimentos/bebel_werner.jpg'),
          'titulo' => "BEBEL E WERNER",
          'texto'  => 'Parabéns pelas fotos do casamento da minha filha Bebel e o Werner, ficaram maravilhosas e realmente tem algumas que só com uma tremenda sensibilidade consegue-se atingir o que foi alcançado!!!  Algumas expressões da Bebel são indescritíveis de tão boas que estão, momento certo, na hora exata você faz um mágico "click" e registra o momento!! Super obrigada pelo maravilhoso trabalho e parabéns!!! Agradeça a todos, foram impecáveis!!! Beijos, Regina (mãe da Bebel)',
          'next' => 8
        ),        

        array(
          'prev' => 7,
          'data' => '03/04/2010',
          'imagens' => array('_imgs/depoimentos/camila_fabio.jpg'),
          'titulo' => "CAMILA E FABIO ",
          'texto'  => 'Turma, estamos simplesmente há 2 horas vendo TODAS as fotos... ficou SENSACIONAL. Muito obrigado! Não há como descrever o profissionalismo de TODOS vocês. Impecáveis! Não temos dúvida que contratamos a melhor equipe do PLANETA para o dia mais feliz de nossas vidas. Desde o início, desde o book, tínhamos essa certeza. Mais que fotógrafos, nossos convidados de honra. Mais convidados, foram cerimonial, psicólogos e etc. (...)  Esperamos e causaremos mais eventos para que tenhamos toda essa turma reunida novamente, como amigos e profissionais. É difícil descrever através das palavras o quão agradecidos e satisfeitos estamos... esperamos ter conseguido repassar um pouquinho da nossa felicidade e gratidão a todos vocês.',
          'next' => 9
        ),        

        array(
          'prev' => 8,
          'data' => '28/11/2009',
          'imagens' => array('_imgs/depoimentos/danielle_caio.jpg'),
          'titulo' => "DANIELLE E CAIO",
          'texto'  => 'Adoramos o trabalho de vocês, mesmo antes de ver as fotos, mas nós não poderíamos ir viajar sem deixar um agradecimento! Não temos palavras para expressar tamanha paciência e compreensão conosco! Vocês são extremamente profissionais, competentes e além de tudo humanos (coisa rara nos dias atuais). Estamos super curiosos para ver as fotos!! Todos os 200 convidados elogiaram o trabalho de vcs! Beijos para todos!!!!',
          'next' => 10
        ),        

        array(
          'prev' => 9,
          'data' => '24/10/2009',
          'imagens' => array('_imgs/depoimentos/regina_tiago.jpg'),
          'titulo' => "REGINA E TIAGO",
          'texto'  => 'Como eu já imaginava, as fotos ficaram FANTÁSTICAS!! Parabéns e muito obrigada por todo cuidado e carinho!',
          'next' => 11
        ),        

        array(
          'prev' => 10,
          'data' => '26/09/2009',
          'imagens' => array('_imgs/depoimentos/daniela_joao_gustavo.jpg'),
          'titulo' => "DANIELA E JOÃO GUSTAVO",
          'texto'  => 'Em algumas horas, meses de preparação passaram... parece triste, mas sabe por que não foi? Porque o olhar de vocês, em cada foto, registraram a emoção sentida a todo momento. Foi BOM D+! MUITO OBRIGADA(O)  cheio de alegria e satisfação! Vocês foram uns amores e muito prestativos! Parabéns! Um grande beijo para vocês, desde Madrid-Spain',
          'next' => 12
        ),

        array(
          'prev' => 11,
          'data' => '11/09/2009',
          'imagens' => array('_imgs/depoimentos/paula_ricardo.jpg'),
          'titulo' => "PAULA E RICARDO",
          'texto'  => 'Não consegui falar com você até agora para dizer o quanto amei meu álbum e o quanto vocês foram incríveis desde o 1º dia que nos vimos!!! MUITO obrigada por me entregarem uma recordação tão linda e que reflete exatamente a energia e a alegria de todos no dia do meu casamento!!! Um beijo enorme e FELIZ ANO NOVO!',
          'next' => 13
        ),

        array(
          'prev' => 12,
          'data' => '09/05/2009',
          'imagens' => array('_imgs/depoimentos/isabelle_pedro.jpg'),
          'titulo' => "ISABELLE E PEDRO",
          'texto'  => 'O trabalho de vocês é incrível, a mistura ideal entre o profissional e o artista. Nosso álbum transmite tudo que vivemos naquele momento tão especial. Obrigada por todo carinho e preparem-se, pois queremos que vocês fotografem nossas próximas comemorações! Beijos a todos da equipe e super obrigada aos que montaram nosso álbum. Seria impossível decidir pelas melhores fotos sem a ajuda deles. Muito sucesso à todos vocês!',
          'next' => 14
        ),

        array(
          'prev' => 13,
          'data' => '22/11/2008',
          'imagens' => array('_imgs/depoimentos/simone_fabio.jpg'),
          'titulo' => "SIMONE E FABIO",
          'texto'  => 'Gostaria de agradecê-los por todo o carinho e dizer o que NOSSO ÁLBUM FICOU MARAVILHOOOOOOOOOOOOOOOOOOOSO! Olho para ele todos os dias e não posso deixar de me emocionar sempre... Vocês captaram imagens inesquecíveis da nossa vida e conseguiram resumir este momento tão mágico da melhor forma possível. Nossos amigos falaram que foi o melhor álbum de casamento que já viram... espero levar muitos clientes para vocês... A família aguarda ansiosamente para vê-lo este fim de semana! Obrigada por tudo, pela simpatia, pela amizade, pela dedicação, paciência, obviamente pelas condições especiais que fizeram e simplesmente por fazer-nos tão felizes quando vemos nossas imagens! Um abraço carinhoso.',
          'next' => 15
        ),

        array(
          'prev' => 14,
          'data' => '23/02/2008',
          'imagens' => array('_imgs/depoimentos/bruna_fabiano.jpg'),
          'titulo' => "BRUNA E FABIANO",
          'texto'  => 'Vim aqui pra dizer que as minhas fotos são a melhor lembrança daquele dia lindo! Eu não me canso de olhá-las pois elas passam exatamente os sentimentos sentidos naquele dia! Não vejo a hora de ter outra oportunidade de fazer novas fotos!',
          'next' => 16
        ),        

        array(
          'prev' => 15,
          'data' => '22/10/2004',
          'imagens' => array('_imgs/depoimentos/lia_fernando.jpg'),
          'titulo' => "LIA E FERNANDO",
          'texto'  => 'Muito obrigada por tudo: Por me deixarem muito tranquila do começo ao fim!!! E por registrarem de forma tão especial um momento tão importante da minha vida! Beijos à equipe e... Muito, muito obrigada.',
          'next' => 'land'
        ));


      if($id == 'land'){

        echo json_encode(array(

          'imagens' => array('_imgs/depoimentos/capa.jpg'),

          'titulo' => "Adoramos OUVIR HISTÓRIAS também!",

          'texto'  => 'Aqui, abrimos espaço para que nossos tão queridos amigos e clientes contem um pouco das suas, e de como ao abrirem suas famílias e histórias para nós, nossos contos entraram onde sempre buscamos estar: no coração. Obrigado por tanto carinho.'

        ));

      }else{

        echo json_encode($deps[$id]);

      }

    }

    function palavras(){

    }



    function contato(){

    }

}

