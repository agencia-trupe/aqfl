<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inspiracoes extends MY_Mobilecontroller {

    function __construct(){
   		parent::__construct();
    }

    function index($abrir = FALSE){

    	$data['aberto'] = $abrir;

    	$data['imagens_celebracoes'] = array(
	        '000085.jpg',
	        '000128-2.jpg',
	        '0049-3.jpg',
	        '0127.jpg',
	        '0214.jpg',
	        '0328.jpg',
	        '0479.jpg',
	        '0640.jpg',
	        '0821.jpg',
	        '0834.jpg',
	        '1116.jpg',
	        '23-3.jpg',
	        'AnnaQuast-002.jpg',
	        'AnnaQuast-003.jpg',
	        'AnnaQuast-006.jpg',
	        'AnnaQuast-007.jpg',
	        'AnnaQuast-012.jpg',
	        'AnnaQuast-013.jpg',
	        'AQ00020017-2.jpg',
	        'AQ00122743-2.jpg',
	        'AQ00130560-2.jpg',
	        'AQ00180304-2.jpg',
	        'AQ00200945.jpg',
	        'AQ00201115.jpg',
	        'AQ00250205-137.jpg',
	        'AQ00251502-815.jpg',
	        'AQ00280903-2.jpg',
	        'AQ00301367-2-2.jpg',
	        'AQ00401980.jpg',
	        'AQ00402033.jpg',
	        'AQ00402052.jpg',
	        'AQ00540699-2.jpg',
	        'AQ00542274.jpg',
	        'AQ00542604.jpg',
	        'AQ00542680.jpg',
	        'AQ00640485.jpg',
	        'AQ00650206-2.jpg',
	        'AQ00680822-2.jpg',
	        'AQ00681089-2.jpg',
	        'AQ00760554-2.jpg',
	        'Barmitsva1-2.jpg',
	        'capa-33X44.jpg',
	        'FL00190156.jpg',
	        'FL0050121-2.jpg',
	        'FL0116 0309.jpg',
	        'FL01230266.jpg',
	        'FL01700124.jpg',
	        'FL02360536 copy.jpg',
	        'FL02840440.jpg',
	        'FL02850996.jpg',
	        'FL02900785.jpg',
	        'FL03130350.jpg',
	        'FL03270357.jpg',
	        'FL03620624-2-2.jpg',
	        'FL03900311.jpg',
	        'FL03990833.jpg',
	        'FL03991828.jpg',
	        'FL04130045.jpg',
	        'FL04330215.jpg',
	        'FL04330740.jpg',
	        'FL04470194.jpg',
	        'FL04500024.jpg',
	        'FL04540275.jpg',
	        'FL04540401.jpg',
	        'FL04541024.jpg',
	        'FL04610487.jpg',
	        'FL04701602.jpg',
	        'FL04780232.jpg',
	        'FL05300303.jpg',
	        'FL05330149.jpg',
	        'FL05350568.jpg',
	        'FL05370262.jpg',
	        'FL05421110.jpg',
	        'FL05421174.jpg',
	        'FL05450811.jpg',
	        'FL05451230-2.jpg',
	        'FL06122217.jpg',
	        'FL06220292.jpg',
	        'FL06220456.jpg',
	        'FL06311233.jpg',
	        'FL06330623.jpg',
	        'FL06330686.jpg',
	        'FL06340596.jpg',
	        'FL06550382.jpg',
	        'FL06550637.jpg',
	        'FL06560407-2.jpg',
	        'FL06560957-2.jpg',
	        'FlaviaGalli_0022.jpg',
	        'LQ00030931-2.jpg',
	        'LQ00031597.jpg',
	        'LQ00080533.jpg',
	        'LQ00080733.jpg',
	        'LQ00080898.jpg',
	        'LQ00080999.jpg',
	        'pag_6-3.jpg',
	        'XFL410-0291.jpg'
    	);

		$data['imagens_expressoes'] = array(
	        '07900001.jpg',
	        'AQ00470314.jpg',
	        'AQ00470475.jpg',
	        'AQ00500042-2.jpg',
	        'AQ00500159.jpg',
	        'AQ0058PARTE30130-2.jpg',
	        'AQ00700081-2.jpg',
	        'CamilaeGui0490.jpg',
	        'FL03580471.jpg',
	        'FL04180456.jpg',
	        'FL06031081.jpg',
	        'FL06250005.jpg',
	        'FL06260387.jpg',
	        'FL06520216.jpg',
	        'LQ00010095-2.jpg',
	        'LQ00010186.jpg',
	        'nina-10.jpg',
	        'rods-5.jpg',
	        'ED69105B.jpg'
		);

   		$this->load->view('mobile/inspiracoes/index', $data);
    }
}