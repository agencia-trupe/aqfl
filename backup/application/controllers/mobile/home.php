<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Mobilecontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
   		$this->load->view('mobile/home');
    }
}