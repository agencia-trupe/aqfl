<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Anna Quast | Fabio Laub - fotografia</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <link rel="stylesheet" href="css/sem-suporte.css">
  
  <!-- <script src="js/libs/modernizr-2.0.6.min.js"></script> -->
</head>

<body>

  <a href="mailto:studio@aqfl.com.br" title="Entre em Contato"><img id="teaser-img" src="_imgs/teaser-aqfl.jpg"></a>
  
  <script>
  /*
    window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
      load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  */
  </script>

</body>
</html>

