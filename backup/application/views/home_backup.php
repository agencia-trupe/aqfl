<div id="loading">
	<div id="Vcenter">Carregando...</div>
	<div id="floatingCirclesG">
		<div class="f_circleG" id="frotateG_01"></div>
		<div class="f_circleG" id="frotateG_02"></div>
		<div class="f_circleG" id="frotateG_03"></div>
		<div class="f_circleG" id="frotateG_04"></div>
		<div class="f_circleG" id="frotateG_05"></div>
		<div class="f_circleG" id="frotateG_06"></div>
		<div class="f_circleG" id="frotateG_07"></div>
		<div class="f_circleG" id="frotateG_08"></div>
	</div>
</div>

<div id="main">

	<div class="tile canv">
		<img class="tile-image" src="_imgs/layout/home2.jpg">
	</div>

	<div class="tile">
		<img class="tile-image" src="_imgs/layout/home1.jpg">
	</div>

	<div class="tile">
		<img class="tile-image pos" src="_imgs/layout/home3.jpg">
	</div>

	<div class="tile">
		<img class="tile-image pos" src="_imgs/layout/home4.jpg">
	</div>

	<div id="menu-container">
		<nav>
			<img src="_imgs/layout/marca.png" alt="AQFL">
			<ul>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Início" id="menu-inicio">Início</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Nós" id="menu-nos">Nós</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Nossas Inspirações" id="menu-inspiracoes">Nossas Inspirações</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Algumas Palavras" id="menu-palavras">Algumas Palavras</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Contato" id="menu-contato">Contato</a><div class='marcador-right'></div></li>
			</ul>
		</nav>
	</div>

	<div id="video-affair">
		<a href="" title="Voltar"><img src="_imgs/layout/marca.png" alt="AQFL"></a>		
	</div>
	
	<div id="bg_slides"></div>

	<div id="logo_link">
		<a href="" title="Voltar"><img src="_imgs/layout/marca.png" alt="AQFL"></a>
	</div>

	<div id="logo_link_inicial">
		<a href="" title="Mostrar Menu"><img src="_imgs/layout/marca.png" alt="AQFL"></a>
	</div>

	<div id="controle_slides">
		<a href="" class="next" title="Próxima Imagem"></a>
		<a href="" class="prev" title="Imagem Anterior"></a>
	</div>

	<div id="texto">
		<div id="texto_overlay"></div>
	</div>

	<div id="texto-depoimentos">
		<div id="dep-nav">
			<a href="" title="Depoimento Anterior" id="dep-prev" data-next=""><img src="_imgs/layout/prev-arrow.png"></a>
			<a href="" title="Próximo Depoimento" id="dep-next" data-next=""><img src="_imgs/layout/next-arrow.png"></a>
		</div>
		<div class="tit"></div>
		<div class="dep"></div>
		<a href="" title="Ver Depoimentos" id="dep-start" data-next="0"><img src="_imgs/layout/plus-sign.png"></a>
	</div>

	<div class="insp-tile first celeb">
		<img src="_imgs/layout/inspiracoes1.jpg">
		<a href="" title="celebrações" id="celebracoes-link">celebrações</a>
	</div>

	<div class="insp-tile expr">
		<img src="_imgs/layout/inspiracoes2.jpg">
		<a href="" title="expressões"  id="expressoes-link">expressões</a>
	</div>

	<div class="insp-tile affa">
		<img src="_imgs/layout/inspiracoes3.jpg">
		<a href="" title="affair" id="affair-link">affair</a>
	</div>
	
	<div class="tile-contato wide">
		<img src="_imgs/layout/contato1.jpg">
	</div>

	<div class="tile-contato">
		<img src="_imgs/layout/contato2.jpg">
	</div>

	<div id="info-contato">
		55 11 3814.4078<br>
		<a href="mailto:studio@aqfl.com.br" title="Entre em Contato">studio@aqfl.com.br</a>
	</div>

	<div id="texto-affair">
		<div id="affair-titulo">
			A FOTOGRAFIA e o MOVIMENTO resolveram<br>ter um caso, e não sabemos mais quem é quem.		
		</div>
		<div class="affair-texto">
			Essa é nossa interpretação mais completa de um dia feliz, de uma família, da vida... São momentos únicos, misturados a sons
			e olhares, a músicas e a soluços. Momentos vivos a partir da fusão de técnicas variadas... Nossa emoção maior.
			<div class="enfase">Um verdadeiro <i>affair</i> entre os sentidos!</div>
			<a href="" title="" id="start-affair">&laquo; CLIQUE AQUI PARA CONHECER &raquo;</a>
		</div>
	</div>

	<div id="completely-hidden"></div>

</div>