
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>


<?if(ENVIRONMENT != 'development'):?>

  <?JS(array(
    'jquery-ui-1.8.12.custom.min',
    'jquery.imagesloaded.min',
    'jquery.mobile.custom.min',
    'bigvideo',
    'easing',
    'transit',
    'jquery.waitforimages',
    'preloader',
    'cycle',
    'funcoes',
  	$this->router->class
  ))?>

<?else:?>

  <?JS(array(
    'funcoes',
    $this->router->class
  ))?>

<?endif;?>

<script defer>
   var BV;
   var isiPad = navigator.userAgent.match(/iPad/i) != null;

  $(function() {
    BV = new $.BigVideo();
    BV.init();
    if (isiPad) {
        BV.show('http://player.vimeo.com/external/49270674.sd.mp4?s=a33d3c1910b62282c0ae19497a809554',{
          autoplay : false,
          poster   : BASE+'_imgs/inicial-poster.jpg',
          controls : true
        });

        $('#main').append($("<img src='_imgs/inicial-poster.jpg' class='vjs-poster' id='dog-play'>"));

        $('#dog-play').click( function(){
          $(this).fadeOut('slow');
          BV.getPlayer().play();
        })

        if(window.location.hash == 'nosound' || window.location.hash == '#nosound'){
          BV.getPlayer().volume(0);
        }
    } else {
        BV.show('http://player.vimeo.com/external/49270674.sd.mp4?s=a33d3c1910b62282c0ae19497a809554',{
          autoplay : false,
          poster   : BASE+'_imgs/inicial-poster.jpg'
        });

        if(window.location.hash == 'nosound' || window.location.hash == '#nosound'){
          BV.getPlayer().volume(0);
        }
    }
    
  });
</script>

</body>
</html>
