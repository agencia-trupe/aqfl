<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>ANNA QUAST &bull; FABIO LAUB</title>
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">
  
  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>
  
  <?CSS(array('reset','video-js', 'base', 'fontface/stylesheet', $this->router->class, $load_css))?>  
  <link rel="stylesheet" href="css/tela_baixa.css" media="screen and (max-height:650px)">
  <link rel="stylesheet" href="css/tela_quadrada.css" media="screen and (max-aspect-ratio:3/2)">


  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array(
    'modernizr-2.0.6.min',
    'less-1.3.0.min',
    'jquery-1.8.0.min',
    'jquery.mobile.custom.min',
    'jquery-ui-1.8.12.custom.min',
    'jquery.imagesloaded.min',
    'video',
    'video.vimeo',
    'bigvideo',
    'easing',
    'transit',
    'blender',
    'jquery.waitforimages',
    'preloader',
    'cycle'
    ))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min'))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>
    <?JS(array('video', 'video.vimeo'))?>

  <?endif;?>

</head>
<body>
