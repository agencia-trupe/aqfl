<div id="loading">
	<div id="Vcenter">Carregando...</div>
	<div id="floatingCirclesG">
		<div class="f_circleG" id="frotateG_01"></div>
		<div class="f_circleG" id="frotateG_02"></div>
		<div class="f_circleG" id="frotateG_03"></div>
		<div class="f_circleG" id="frotateG_04"></div>
		<div class="f_circleG" id="frotateG_05"></div>
		<div class="f_circleG" id="frotateG_06"></div>
		<div class="f_circleG" id="frotateG_07"></div>
		<div class="f_circleG" id="frotateG_08"></div>
	</div>
</div>

<div id="main">

	<!-- BOTÃO DA MARCA - PARA FECHAR GALERIAS -->
	<div id="link-logo" class="hid">
		<a href="#" title="Voltar ao Menu"><img src="_imgs/layout/marca.png"></a>
	</div>

	<!-- MENU -->
	<div id="menu-container" class="hid">
		<nav>
			<img src="_imgs/layout/marca.png" alt="AQFL">
			<ul>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Início" id="menu-inicio">Início</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Nós" id="menu-nos">Nós</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Nossas Inspirações" id="menu-inspiracoes">Nossas Inspirações</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Algumas Palavras" id="menu-palavras">Algumas Palavras</a><div class='marcador-right'></div></li>
				<li><div class='marcador-left'></div><a href="" class="menu-link" title="Contato" id="menu-contato">Contato</a><div class='marcador-right'></div></li>
			</ul>
		</nav>
	</div>




	<!-- VÍDEO INTRO -->
	<div id="logo_link_inicial"><a href="" title="Mostrar Menu"><img src="_imgs/layout/marca.png" alt="AQFL"></a></div>

	<!-- VÍDEO AFFAIR -->
	<div id="video-affair"><a href="" title="Voltar"><img src="_imgs/layout/marca.png" alt="AQFL"></a></div>




	<!-- TEXTO NÓS -->
	<div class="texto hid" id="texto-nos">
		<div class='texto-left animate'>
			<span class='f-42'>Lente, ângulo, luz e ambiente</span><br>são capazes de tornar uma imagem mais bela...<br>MAS <span class='f-42'>NENHUM DELES</span> TEM A FORÇA<br>DE REVELAR <span class='f-42'>SENTIMENTO</span>.
		</div>
		<div class='texto-right animate'>
			Imagens vivas precisam de um fotógrafo que sinta, muito mais do que veja. Tem que gostar de gente e de se envolver de verdade com suas histórias. Nós, Anna e Fabio, definimos assim nossa fotografia, e não temos dúvida de que essa sintonia de pensamentos e propósitos foi o que nos aproximou e uniu nossos trabalhos. Um projeto que só tem sentido porque é feito de pessoas apaixonadas pelo que fazem para pessoas e histórias apaixonantes. E é por isso que, mais do que esperar, desejamos sua visita.  Venha tomar um café conosco. Estamos ansiosos para ouvir suas histórias.
		</div>	
	</div>	

	<!-- TEXTO DEPOIMENTOS -->
	<div class="texto hid" id="texto-depoimentos">
		<div id="dep-nav">
			<a href="#" title="Depoimento Anterior" id="dep-prev" data-next=""><img src="_imgs/layout/prev-arrow.png"></a>
			<a href="#" title="Próximo Depoimento" id="dep-next" data-next=""><img src="_imgs/layout/next-arrow.png"></a>
		</div>
		<div class="tit">Adoramos OUVIR HISTÓRIAS também!</div>
		<div class="dep">Aqui, abrimos espaço para que nossos tão queridos amigos e clientes contem um pouco das suas, e de como ao abrirem suas famílias e histórias para nós, nossos contos entraram onde sempre buscamos estar: no coração. Obrigado por tanto carinho.</div>
		<a href="#" title="Ver Depoimentos" id="dep-start" data-next="0"><img src="_imgs/layout/plus-sign.png"></a>
	</div>

	<!-- TEXTO AFFAIR -->
	<div class="texto hid" id="texto-affair">
		<div id="affair-titulo">
			A FOTOGRAFIA e o MOVIMENTO resolveram<br>ter um caso, e não sabemos mais quem é quem.		
		</div>
		<div class="affair-texto">
			Essa é nossa interpretação mais completa de um dia feliz, de uma família, da vida... São momentos únicos, misturados a sons
			e olhares, a músicas e a soluços. Momentos vivos a partir da fusão de técnicas variadas... Nossa emoção maior.
			<div class="enfase">Um verdadeiro <i>affair</i> entre os sentidos!</div>
			<a href="" title="" id="start-affair">&laquo; CLIQUE AQUI PARA CONHECER &raquo;</a>
		</div>
	</div>

	<!-- TEXTO CONTATO -->
	<div id="info-contato" class="hid">
		55 11 3814.4078<br>
		<a href="mailto:studio@aqfl.com.br" title="Entre em Contato">studio@aqfl.com.br</a>
	</div>




	<!-- GALERIA NÓS -->
	<div class="galeria hid" id="galeria-nos">
		<img src="_imgs/slides/slide1.jpg">
	</div>

	<!-- GALERIA AFFAIR -->
	<div class="galeria hid" id="galeria-affair">
		<img src="_imgs/layout/affair.jpg">	
	</div>

	<!-- GALERIA DEPOIMENTOS -->
	<div class="galeria hid" id="galeria-depoimentos">
		<img src="_imgs/depoimentos/capa.jpg">		
	</div>

	<!-- GALERIA CELEBRAÇÕES -->
	<div class="galeria hid" id="galeria-celebracoes"></div>

	<!-- GALERIA EXPRESSÕES -->
	<div class="galeria hid" id="galeria-expressoes"></div>

	<!-- CONTROLE DAS GALERIAS -->
	<div id="controle_slides">
		<a href="" class="next" title="Próxima Imagem"></a>
		<a href="" class="prev" title="Imagem Anterior"></a>
	</div>




	<!-- TILES INICIAL -->
	<div class="tiles hid" id="tiles-inicio">
		<!-- Sem espaços entre os .tile para não criar espaçamento de inline-block no Safari -->
		<div class="tile primeiro" id="imagem-noiva"></div><div class="tile" id="imagem-alianca"></div><div class="tile" id="imagem-altar"></div><div class="tile" id="imagem-noivos"></div>
	</div>

	<!-- TILES INSPIRAÇÕES -->
	<div class="tiles hid" id="tiles-inspiracoes">
		<!-- Sem espaços entre os .tile para não criar espaçamento de inline-block no Safari -->
		<div class="tile primeiro hid" id="tile-celebracoes"><a href="#" title="celebrações" id="celebracoes-link">celebrações</a></div><div class="tile hid" id="tile-expressoes"><a href="#" title="expressões"  id="expressoes-link">expressões</a></div><div class="tile hid" id="tile-affair"><a href="#" title="affair" id="affair-link">affair</a></div>
	</div>

	<!-- TILES CONTATO -->
	<div class="tiles hid" id="tiles-contato">
		<div class="tile primeiro hid" id="contato-esquerda"><!-- Imagem de Contato 1 --></div>
		<div class="tile segundo hid" id="contato-direita"><!-- Imagem de Contato 2 --></div>
	</div>


</div>