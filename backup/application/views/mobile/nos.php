<div class='texto-variable-size'>
	<span class='f-42'>Lente, ângulo, luz e ambiente</span><br>são capazes de tornar uma imagem mais bela...<br>MAS <span class='f-42'>NENHUM DELES</span> TEM A FORÇA<br>DE REVELAR <span class='f-42'>SENTIMENTO</span>.
</div>

<img src="_imgs/mobile/nos.jpg" alt="Anna Quast - Fabio Laub">

<p>
	Imagens vivas precisam de um fotógrafo que sinta, muito mais do que veja. Tem que gostar de gente e de se envolver de verdade com suas histórias. Nós, Anna e Fabio, definimos assim nossa fotografia, e não temos dúvida de que essa sintonia de pensamentos e propósitos foi o que nos aproximou e uniu nossos trabalhos. Um projeto que só tem sentido porque é feito de pessoas apaixonadas pelo que fazem para pessoas e histórias apaixonantes. E é por isso que, mais do que esperar, desejamos sua visita.  Venha tomar um café conosco. Estamos ansiosos para ouvir suas histórias.
</p>