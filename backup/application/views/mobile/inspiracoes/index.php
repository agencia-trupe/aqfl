<?if($aberto == 'celebracoes'):?>

	<h1><div id='prev'><img src="_imgs/mobile/seta_esq.png"></div>celebrações<div id='next'><img src="_imgs/mobile/seta_dir.png"></div></h1>

	<div class="texto-celebracoes animate">
		<?php foreach ($imagens_celebracoes as $key => $value): ?>
			<img src="_imgs/mobile/galerias/celebracoes/<?=$value?>" <?if($key > 0)echo" style='display:none;'"?>>
		<?php endforeach ?>
	</div>

<?else:?>

	<a class="link-abrir" href="mobile/inspiracoes/index/celebracoes" title="Celebrações">celebrações</a>

<?endif;?>

<!-- ============================== -->

<?if($aberto == 'expressoes'):?>

	<h1><a href="#" id='prev'><img src="_imgs/mobile/seta_esq.png"></a>expressões<a href="#" id='next'><img src="_imgs/mobile/seta_dir.png"></a></h1>

	<div class="texto-expressoes animate">
		<?php foreach ($imagens_expressoes as $key => $value): ?>
			<img src="_imgs/mobile/galerias/expressoes/<?=$value?>" <?if($key > 0)echo" style='display:none;'"?>>
		<?php endforeach ?>
	</div>

<?else:?>

	<a class="link-abrir" href="mobile/inspiracoes/index/expressoes" title="Expressões">expressões</a>

<?endif;?>

<!-- ============================== -->

<?if($aberto == 'affair'):?>

	<h1>affair</h1>

	<img src="_imgs/mobile/affair.jpg">

	<div class="texto-affair-1">
		A FOTOGRAFIA e o MOVIMENTO resolveram<br>ter um caso, e não sabemos mais quem é quem.
	</div>

	<div class="texto-affair-2">
		Essa é nossa interpretação mais completa de um dia feliz, de uma família, da vida... São momentos únicos, misturados a sons e olhares, a músicas e a soluços. Momentos vivos a partir da fusão de técnicas variadas... Nossa emoção maior.
	</div>

	<div class="texto-affair-3">
		Um verdadeiro <i>affair</i> entre os sentidos!
	</div>

	<video id="video" class='video-js vjs-default-skin' preload='auto' controls data-setup='{}' poster='_imgs/mobile/poster_affair.png'>
		<source src='http://player.vimeo.com/external/49263155.sd.mp4?s=19ad86880756a19620816170fa7795c6' type='video/mp4'>
	</video>

<?else:?>

	<a class="link-abrir" href="mobile/inspiracoes/index/affair" title="Affair">affair</a>

<?endif;?>