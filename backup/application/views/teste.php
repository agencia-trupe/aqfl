<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title></title>
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <style type="text/css">
  body{
    width:100%;
    height:100%;
    overflow:hidden;
  }
  </style>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', $this->router->class))?>  
  <?JS(array('modernizr-2.0.6.min','less-1.3.0.min','jquery-1.8.0.min','jquery-ui-1.8.12.custom.min','jquery.imagesloaded.min','video','bigvideo'))?>
    

</head>
<body>

  

  
  <script defer>
  
  $(function() {
    var BV = new $.BigVideo();
    BV.init();
    BV.show('http://player.vimeo.com/external/49263155.sd.mp4?s=19ad86880756a19620816170fa7795c6', {ambient:true});
  });

  </script>
  
</body>
</html>
