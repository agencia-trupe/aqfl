<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar;
    var $footervar;
    var $menuvar;

    function __construct($css = '', $js = '') {
        parent::__construct();

        $this->load->library('user_agent');

        if($this->agent->is_mobile() || ($this->agent->browser() == 'Internet Explorer' && $this->agent->version() <= (int) 8)){

            if($this->agent->browser() == 'Internet Explorer' && $this->agent->version() <= (int) 6){
                redirect('ie8');
            }

            $is_iPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');

            if (!$is_iPad)
                redirect('mobile/home');
        }

        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        //$this->output->enable_profiler(TRUE);
    }
    
    // function _output($output){
    //     echo $this->load->view('common/header', $this->headervar, TRUE).
    //          $this->load->view('common/menu', $this->menuvar, TRUE).
    //          $output.
    //          $this->load->view('common/footer', $this->footervar, TRUE);
    // }

    function _output($output){
        echo $this->load->view('common/header', $this->headervar, TRUE).
             $output.
             $this->load->view('common/footer', $this->footervar, TRUE);
    }

}
?>