<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Mobilecontroller extends CI_controller {

    var $headervar;
    var $footervar;
    var $menuvar;

    function __construct($css = '', $js = '') {
        parent::__construct();

        $this->load->library('user_agent');

        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        //$this->output->enable_profiler(TRUE);
    }
    
    function _output($output){
        echo $this->load->view('mobile/common/header', $this->headervar, TRUE).
             $this->load->view('mobile/common/menu', $this->menuvar, TRUE).
             $output.
             $this->load->view('mobile/common/footer', $this->footervar, TRUE);
    }

}
?>