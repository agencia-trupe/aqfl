<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inspiracoes extends MY_Mobilecontroller {

    function __construct(){
   		parent::__construct();
    }

    function index($abrir = FALSE){

    	$data['aberto'] = $abrir;

    	$data['imagens_celebracoes'] = $this->db->order_by('ordem', 'asc')->get('imagens_celebracoes')->result();
    	
		$data['imagens_expressoes'] = $this->db->order_by('ordem', 'asc')->get('imagens_expressoes')->result();

   		$this->load->view('mobile/inspiracoes/index', $data);
    }
}