<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imagens_expressoes extends MY_Admincontroller {

    function __construct(){
        parent::__construct();

        $this->titulo = 'Imagens - Expressões';
        $this->unidade = 'Imagem';
        $this->load->model('imagens_expressoes_model', 'model');
    }

}