<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Depoimentos extends MY_Admincontroller {

    function __construct(){
        parent::__construct();

        $this->titulo = 'Depoimentos';
        $this->unidade = 'Depoimento';
        $this->load->model('depoimentos_model', 'model');
    }
}