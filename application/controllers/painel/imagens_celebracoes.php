<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imagens_celebracoes extends MY_Admincontroller {

    function __construct(){
        parent::__construct();

        $this->titulo = 'Imagens - Celebrações';
        $this->unidade = 'Imagem';
        $this->load->model('imagens_celebracoes_model', 'model');
    }
}