<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Depoimentos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'depoimentos';

		$this->dados = array('casal' ,'arquivo', 'texto', 'ordem');
		$this->dados_tratados = array(
			'arquivo' => $this->sobeImagem(),
			'texto' => nl2br($this->input->post('texto'))
		);		
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/depoimentos/',
			'x' => '2000',
			'y' => '1000',
			'corte' => 'resize',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '',
		  'max_width' => '2000',
		  'max_height' => '1000');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
		        if(isset($original['x']) && $original['x'] && isset($original['y']) && $original['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->$original['corte']($original['x'], $original['y'])
		                ->save($original['dir'].$filename, TRUE);
		        }

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function pegarTodos(){
		return $this->db->order_by('ordem', 'asc')->get($this->tabela)->result();
	}

	function pegarItem($id){

		$query = $this->db->get_where('depoimentos', array('ordem' => $id), 1, 0)->result();

		foreach ($query as $key => $value) {
			$value->next = $this->pegaProximo($value->ordem);
			$value->prev = $this->pegaAnterior($value->ordem);
		}

		return $query[0];
	}

	function pegaProximo($id){
		$query = $this->db->query("SELECT * FROM depoimentos WHERE ordem > $id ORDER BY id ASC LIMIT 1")->result();
		if(isset($query[0]))
			return $query[0]->ordem;
		else
			return 'land';
	}

	function pegaAnterior($id){
		$query = $this->db->query("SELECT * FROM depoimentos WHERE ordem < $id ORDER BY id DESC LIMIT 1")->result();
		if(isset($query[0]))
			return $query[0]->ordem;
		else
			return 'land';
	}

}