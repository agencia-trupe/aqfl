<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imagens_celebracoes_model extends MY_Model {

	var $tabela;

	function __construct(){
		parent::__construct();

		$this->tabela = "imagens_celebracoes";

		$this->dados = array('arquivo', 'ordem');
		$this->dados_tratados = array(
			'arquivo' => $this->sobeImagem()
		);		
	}

	function sobeImagem(){
		$this->load->library('upload');
		
		$pasta = 'celebracoes';
		
		$original = array(
			'dir' => '_imgs/galerias/'.$pasta.'/',
			'x' => '2000',
			'y' => '1000',
			'corte' => 'resize',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '',
		  'max_width' => '2000',
		  'max_height' => '1000');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'], $original['dir'].$filename);
		        
	        	$this->image_moo
	                ->load($original['dir'].$filename)
	                ->$original['corte']($original['x'], $original['y'])
	                ->save($original['dir'].$filename, TRUE)
	                ->resize(768, 99999)
	                ->save('_imgs/mobile/galerias/'.$pasta.'/'.$filename, TRUE);
		        
		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function pegarTodos(){
		return $this->db->order_by('ordem', 'asc')->order_by('id', 'asc')->get($this->tabela)->result();
	}	

	function _setTabela($tabela){
		$this->tabela = $tabela;
	}

}