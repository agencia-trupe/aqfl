<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>ANNA QUAST &bull; FABIO LAUB</title>
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>
  <link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
  <script src="http://vjs.zencdn.net/c/video.js"></script>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', $this->router->class))?>  
  

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array(
    'modernizr-2.0.6.min',
    'less-1.3.0.min',
    'jquery-1.8.0.min'))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min'))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>

  <canvas id="backdrop"></canvas>

  <div id="wrapper">

    <canvas id="fundo-azul"></canvas>

    <div class="center">
      
      <img src="_imgs/layout/marca.png" alt="AQFL" id="logo">

      <div class="trilha">Acreditamos que a vida tem trilha sonora.</div>

      <a href="home" title="navegar com som" class="enter-btn" id="sound">&raquo; navegar com som &laquo;</a><a href="home#nosound" class="enter-btn" title="navegar sem som" id="no-sound">&raquo; navegar sem som &laquo;</a>

      <div class="assinatura">&copy; <?=date('Y')?> Anna Quast Fabio Laub - Todos os direitos reservados | <a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa">Criação de sites: Trupe Agência Criativa</a></div>

    </div>

  </div>


  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('blender', 'front', $this->router->class))?>
  
</body>
</html>
