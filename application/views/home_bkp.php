<div id="loading">
	<div id="Vcenter">Carregando...</div>
</div>

<div id="main">

	<img class="tile tile-image" id="tile-first" src="_imgs/layout/home1.jpg">
	<canvas class="tile canv" id="tile-canvas"></canvas>
	<img class="tile tile-image pos" src="_imgs/layout/home3.jpg">
	<img class="tile tile-image pos" src="_imgs/layout/home4.jpg">

	<div id="menu-container">
		<canvas id="menu-canvas"></canvas>
		<nav>
			<img src="_imgs/layout/marca.png" alt="AQFL">

			<ul>
				<li><a href="" title="Nós">Nós</a></li>
				<li><a href="" title="Nossas Inspirações">Nossas Inspirações</a></li>
				<li><a href="" title="Algumas Palavras">Algumas Palavras</a></li>
				<li><a href="" title="Contato">Contato</a></li>
			</ul>
		</nav>
	</div>

	<div id="video">
		<video muted id="bg_video" class="first_set">
		  <source src="video/video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
		  <source src="video/video.webm" type='video/webm; codecs="vp8, vorbis"' />
		</video>
	</div>
	
</div>