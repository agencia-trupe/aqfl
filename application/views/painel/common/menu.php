
<!-- Shell -->
<div id="shell">
	
	<!-- Header -->
	<div id="header">
		<h1><a href="<?=base_url('painel/home')?>">Painel - <?=CLIENTE?></a></h1>
		<div class="right">
			<p>Bem vindo, <strong><?=$this->session->userdata('username')?></strong></p>
			<p class="small-nav"><a href="<?=base_url('painel/usuarios')?>" <?if($this->router->class=='usuarios')echo " class='active'"?>>Usuários</a> / <a href="<?=base_url('painel/home/logout')?>">Log Out</a></p>
		</div>
	</div>
	<!-- End Header -->
	
	<!-- Navigation -->
	<div id="navigation">
		<ul>
		    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
		    <li><a href="<?=base_url('painel/imagens_celebracoes')?>" <?if($this->router->class == 'imagens_celebracoes')echo " class='active'"?>><span>Imagens - Celebrações</span></a></li>
		    <li><a href="<?=base_url('painel/imagens_expressoes')?>" <?if($this->router->class == 'imagens_expressoes')echo " class='active'"?>><span>Imagens - Expressões</span></a></li>
		    <li><a href="<?=base_url('painel/depoimentos')?>" <?if($this->router->class == 'depoimentos')echo " class='active'"?>><span>Depoimentos</span></a></li>
		</ul>
	</div>
	<!-- End Navigation -->
	
	<!-- Content -->
	<div id="content">

	<?if(isset($mostrarerro_mensagem) && $mostrarerro_mensagem):?>
		<div class="mensagem" id="erro"><?=$mostrarerro_mensagem?></div>
	<?elseif(isset($mostrarsucesso_mensagem) && $mostrarsucesso_mensagem):?>
		<div class="mensagem" id="sucesso"><?=$mostrarsucesso_mensagem?></div>
	<?endif;?>