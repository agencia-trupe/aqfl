<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir <?=$unidade?></a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Imagem</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

	</table>

	<ul class="resultados">

		<? foreach ($registros as $key => $value): ?>

			<li class="tr-row" id="row_<?=$value->id?>">
				<img src="_imgs/galerias/expressoes/<?=$value->arquivo?>" style="max-width:200px;">
				<a class="move" href="#">Ordenar</a>
				<a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a>
			</li>
				
		<? endforeach; ?>

	</ul>

<?else:?>

	<h2>Nenhuma Imagem Cadastrada</h2>

<?endif;?>

<style type="text/css">
	.resultados .tr-row{
		text-align:center;
		display: inline-block;
		*display:inline;
		zoom:1;
		vertical-align:top;
		width:240px;
		margin:5px;
	}
	.resultados .tr-row img{
		display:block;
		margin:3px auto;
	}
</style>

<script defer>

	$('document').ready( function(){

		$('.move').click( function(e){ e.preventDefault(); })

	    $(".resultados").sortable({
	        update : function () {
	            serial = [];
	            $('.resultados').children('.tr-row').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'imagens_expressoes'}, function(retorno){
	            	//console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>