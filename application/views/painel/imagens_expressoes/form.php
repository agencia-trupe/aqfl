<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir <?=$unidade?></a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Imagem (Reduzida)<br>
			<div id="alerta" class="mensagem" style="display:block; font-size:11px; width:300px;margin-bottom:5px;">
				Tamanho Ideal da imagem: 2000px x 1000px
			</div>
		<?php if ($registro->arquivo): ?>
			<img src="_imgs/galerias/expressoes/<?=$registro->arquivo?>" style="width:400px;"><br>
		<?php endif ?>
		<input type="file" name="userfile"></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Imagem<br>
			<div id="alerta" class="mensagem" style="display:block; font-size:11px; width:300px;margin-bottom:5px;">
				Tamanho Ideal da imagem: 2000px x 1000px
			</div>
		<input type="file" name="userfile"></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>