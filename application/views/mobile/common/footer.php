
  </div> <!-- fim da div main -->

  <footer>
  
  </footer>
  
  
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('jquery.mobile.custom.min', 'jquery.fittext', 'cycle', 'video', 'mobile'))?>
  
</body>
</html>
