<header>

	<a href="<?=base_url()?>mobile/home" id="link-home" title="Página Inicial"><img src="_imgs/mobile/logo.png" alt="Anna Quast - Fabio Laub"></a>

	<nav>
		<ul>
			<li><a data-role="none" href="mobile/nos" title="Nós" id="mn-nos" <?if($this->router->class=='nos')echo" class='ativo'"?>>Nós</a></li>
			<li><a data-role="none" href="mobile/inspiracoes" title="Nossas Inspirações" id="mn-inspiracoes" <?if($this->router->class=='inspiracoes')echo" class='ativo'"?>>Nossas Inspirações</a></li>
			<li><a data-role="none" href="mobile/contato" title="Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>Contato</a></li>
		</ul>		
	</nav>
	
</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">